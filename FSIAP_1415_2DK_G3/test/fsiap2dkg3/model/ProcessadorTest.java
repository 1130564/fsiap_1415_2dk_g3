/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gilbertopereira
 */
public class ProcessadorTest {

    public ProcessadorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class Processador.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Processador instance = new Processador("Intel", 1, 2, 3);
        String expResult = "Nome: Intel\n"
                + "(TDP)Potencia Dissipada: 1.0w\n"
                + "Termperatura Maxima do Processador: 2.0ºC\n"
                + "Area: 3.0m^2";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Processador.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object outro = new Processador();;
        Processador instance = new Processador();
        boolean expResult = true;
        boolean result = instance.equals(outro);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Processador.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Processador instance = new Processador("Intel", 1, 2, 3);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

}
