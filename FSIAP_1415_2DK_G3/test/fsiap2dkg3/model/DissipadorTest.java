/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Emanuel Marques
 */
public class DissipadorTest {
    
    public DissipadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

   

  
    /**
     * Test of equals method, of class Dissipador.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Dissipador outro = new Dissipador();
        Dissipador instance = new Dissipador();
        boolean expResult = true;
        boolean result = instance.equals(outro);
        assertEquals(expResult, result);
   
    }

    /**
     * Test of valida method, of class Dissipador.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Dissipador instance = new Dissipador("teste", "teste", 1, 1);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }
    
}
