/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paulo
 */
public class PastaTermicaTest {
    
    public PastaTermicaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class PastaTermica.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        PastaTermica instance = new PastaTermica();
        //cria pasta com valores não definido, cond. termica 0.0, espessura 0.0, teste ao to string.
        String expResult = "Nome: não definido\nCondutividade Termica: 0.0W/m.c\nEspessura: 0.0mm";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class PastaTermica.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object outro = null;
        PastaTermica instance = new PastaTermica();
        boolean expResult = false;
        boolean result = instance.equals(outro);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class PastaTermica.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        PastaTermica instance = new PastaTermica();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    
}
