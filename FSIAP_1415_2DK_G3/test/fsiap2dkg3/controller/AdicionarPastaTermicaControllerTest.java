/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.model.PastaTermica;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paulo
 */
public class AdicionarPastaTermicaControllerTest {
    
    public AdicionarPastaTermicaControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of novoPastaTermica method, of class AdicionarPastaTermicaController.
     */
    @Test
    public void testNovoPastaTermica() {
        System.out.println("novoPastaTermica");
        String nome = "pasta";
        float condutividadeTermica = 1.5F;
        float espessura = 1.5F;
        AdicionarPastaTermicaController instance = new AdicionarPastaTermicaController();
        PastaTermica expResult = new PastaTermica("pasta",1.5f,1.5f);
        PastaTermica result = instance.novoPastaTermica(nome, condutividadeTermica, espessura);
        assertEquals(expResult, result);
    }

    /**
     * Test of registaPastaTermica method, of class AdicionarPastaTermicaController.
     */
    @Test
    public void testRegistaPastaTermica() {
        System.out.println("registaPastaTermica");
        AdicionarPastaTermicaController instance = new AdicionarPastaTermicaController();
        instance.novoPastaTermica("pasta",1f,1f);
        boolean expResult = true;
        boolean result = instance.registaPastaTermica();
        assertEquals(expResult, result);
    }
    
}
