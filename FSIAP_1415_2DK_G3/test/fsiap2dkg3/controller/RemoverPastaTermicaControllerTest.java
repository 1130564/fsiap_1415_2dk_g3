/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.model.PastaTermica;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paulo
 */
public class RemoverPastaTermicaControllerTest {
    
    public RemoverPastaTermicaControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of removerPastaTermica method, of class RemoverPastaTermicaController.
     */
    @Test
    public void testRemoverPastaTermica() {
        System.out.println("removerPastaTermica");
        String nome1 = "teste";
        float condutividadeTermica = 1.0F;
        float espessura1 = 1.0F;
        AdicionarPastaTermicaController instanceAdicionar = new AdicionarPastaTermicaController();
        
        PastaTermica p = instanceAdicionar.novoPastaTermica(nome1, condutividadeTermica, espessura1);
        RemoverPastaTermicaController instanceRemover = new RemoverPastaTermicaController();
        instanceAdicionar.registaPastaTermica();
        instanceRemover.setPastaTermica(p);
        boolean expResult = true;
        boolean result = instanceRemover.removerPastaTermica();
        assertEquals(expResult, result);   
        
    }
    
}
