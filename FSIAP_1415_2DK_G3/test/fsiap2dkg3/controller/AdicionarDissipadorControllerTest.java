/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.model.Dissipador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Emanuel Marques
 */
public class AdicionarDissipadorControllerTest {

    public AdicionarDissipadorControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of novoDissipador method, of class AdicionarDissipadorController.
     */
    @Test
    public void testNovoDissipador() {
        System.out.println("novoDissipador");
        String nome = "teste";
        String tipo = "teste";
        float condutividadeTermica = 1.0F;
        float espessura = 1.0F;
        AdicionarDissipadorController instance = new AdicionarDissipadorController();
        Dissipador expResult = new Dissipador("teste", "teste", 1, 1);
        Dissipador result = instance.novoDissipador(nome, tipo, condutividadeTermica, espessura);
        assertEquals(expResult, result);

    }

    /**
     * Test of registaDissipador method, of class AdicionarDissipadorController.
     */
    @Test
    public void testRegistaDissipador() {
        System.out.println("registaDissipador");
        AdicionarDissipadorController instance = new AdicionarDissipadorController();
         String nome = "teste";
        String tipo = "teste";
        float condutividadeTermica = 1.0F;
        float espessura = 1.0F;
        instance.novoDissipador(nome, tipo, condutividadeTermica, espessura);
        boolean expResult = true;
        boolean result = instance.registaDissipador();
        assertEquals(expResult, result);

    }

}
