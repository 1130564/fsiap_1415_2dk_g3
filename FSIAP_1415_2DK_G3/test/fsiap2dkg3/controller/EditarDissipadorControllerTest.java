/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.model.Dissipador;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Emanuel Marques
 */
public class EditarDissipadorControllerTest {
    
    public EditarDissipadorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of editDissipador method, of class EditarDissipadorController.
     */
    @Test
    public void testEditDissipador() {
        System.out.println("editDissipador");
        String nome = "teste2";
        String tipo = "teste2";
        float condutividade = 2.0F;
        float espessura = 2.0F;
        AdicionarDissipadorController instance2 = new AdicionarDissipadorController();
         String nome1 = "teste";
        String tipo1 = "teste";
        float condutividadeTermica = 1.0F;
        float espessura1 = 1.0F;
        Dissipador d = instance2.novoDissipador(nome1, tipo1, condutividadeTermica, espessura1);
        instance2.registaDissipador();
        EditarDissipadorController instance = new EditarDissipadorController();
        instance.setDissipador(d);
        boolean expResult = true;
        boolean result = instance.editDissipador(nome, tipo, condutividade, espessura);
        assertEquals(expResult, result);

    }
    
}
