/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.model.PastaTermica;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paulo
 */
public class EditarPastaTermicaControllerTest {
    
    public EditarPastaTermicaControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /**
     * Test of editPastaTermica method, of class EditarPastaTermicaController.
     */
    @Test
    public void testEditPastaTermica() {
        
        System.out.println("editPastaTermica");
        String nome = "teste2";
        float condutividade = 2.0F;
        float espessura = 2.0F;
        AdicionarPastaTermicaController instance = new AdicionarPastaTermicaController();
        String nome1 = "teste";
        float condutividadeTermica = 1.0F;
        float espessura1 = 1.0F;
        PastaTermica p = instance.novoPastaTermica(nome1, condutividadeTermica, espessura1);
        instance.registaPastaTermica();
        EditarPastaTermicaController instance2 = new EditarPastaTermicaController();
        instance2.setPastaTermica(p);
        boolean expResult = true;
        boolean result = instance2.editPastaTermica(nome, condutividade, espessura);
        assertEquals(expResult, result);
    }
    
}
