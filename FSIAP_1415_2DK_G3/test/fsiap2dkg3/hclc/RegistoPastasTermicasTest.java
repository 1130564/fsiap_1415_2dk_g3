/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.hclc;

import fsiap2dkg3.model.PastaTermica;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paulo
 */
public class RegistoPastasTermicasTest {
    
    public RegistoPastasTermicasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getPastasTermicas method, of class RegistoPastasTermicas.
     */
    @Test
    public void testGetPastasTermicas() {
        System.out.println("getPastasTermicas");
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        List<PastaTermica> expResult = null;
        List<PastaTermica> result = instance.getPastasTermicas();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testGetPastaTermica() {
        System.out.println("getPastaTermica");
        int pos = 0;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        PastaTermica expResult = null;
        PastaTermica result = instance.getPastaTermica(pos);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPastasTermicas method, of class RegistoPastasTermicas.
     */
    @Test
    public void testSetPastasTermicas() {
        System.out.println("setPastasTermicas");
        List<PastaTermica> listaPastasTermicas = null;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        instance.setPastasTermicas(listaPastasTermicas);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class RegistoPastasTermicas.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of equals method, of class RegistoPastasTermicas.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object outro = null;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        boolean expResult = false;
        boolean result = instance.equals(outro);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testAddPastaTermica() {
        System.out.println("addPastaTermica");
        PastaTermica o = null;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        boolean expResult = false;
        boolean result = instance.addPastaTermica(o);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of rmPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testRmPastaTermica_PastaTermica() {
        System.out.println("rmPastaTermica");
        PastaTermica o = null;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        boolean expResult = false;
        boolean result = instance.rmPastaTermica(o);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of rmPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testRmPastaTermica_int() {
        System.out.println("rmPastaTermica");
        int pos = 0;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        PastaTermica expResult = null;
        PastaTermica result = instance.rmPastaTermica(pos);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of replacePastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testReplacePastaTermica() {
        System.out.println("replacePastaTermica");
        PastaTermica old = null;
        PastaTermica novo = null;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        boolean expResult = false;
        boolean result = instance.replacePastaTermica(old, novo);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of editPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testEditPastaTermica_4args_1() {
        System.out.println("editPastaTermica");
        int pos = 0;
        String nome = "";
        float condutividadeTermica = 0.0F;
        float espessura = 0.0F;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        boolean expResult = false;
        boolean result = instance.editPastaTermica(pos, nome, condutividadeTermica, espessura);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of editPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testEditPastaTermica_4args_2() {
        System.out.println("editPastaTermica");
        PastaTermica o = new PastaTermica("a",1,1);
        String nome = "";
        float condutividadeTermica = 0.0F;
        float espessura = 0.0F;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        boolean expResult = true;
        boolean result = instance.editPastaTermica(o, nome, condutividadeTermica, espessura);
        assertEquals(expResult, result);
    }

    /**
     * Test of numeroPastasTermicas method, of class RegistoPastasTermicas.
     */
    @Test
    public void testNumeroPastasTermicas() {
        System.out.println("numeroPastasTermicas");
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        int expResult = 0;
        int result = instance.numeroPastasTermicas();
        assertEquals(expResult, result);
    }

    /**
     * Test of indexOfPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testIndexOfPastaTermica() {
        System.out.println("indexOfPastaTermica");
        PastaTermica o = null;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        int expResult = 0;
        int result = instance.indexOfPastaTermica(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of contemPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testContemPastaTermica() {
        System.out.println("contemPastaTermica");
        PastaTermica o = null;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        instance.addPastaTermica(o);
        boolean expResult = true;
        boolean result = instance.contemPastaTermica(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of validaPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testValidaPastaTermica() {
        System.out.println("validaPastaTermica");
        PastaTermica o = null;
        RegistoPastasTermicas instance = new RegistoPastasTermicas();
        instance.addPastaTermica(o);
        boolean expResult = true;
        boolean result = instance.validaPastaTermica(o);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of novoPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testNovoPastaTermica_0args() {
        System.out.println("novoPastaTermica");
        PastaTermica expResult = null;
        PastaTermica result = RegistoPastasTermicas.novoPastaTermica();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of novoPastaTermica method, of class RegistoPastasTermicas.
     */
    @Test
    public void testNovoPastaTermica_3args() {
        System.out.println("novoPastaTermica");
        String nome = "";
        float condutividadeTermica = 0.0F;
        float espessura = 0.0F;
        PastaTermica expResult = null;
        PastaTermica result = RegistoPastasTermicas.novoPastaTermica(nome, condutividadeTermica, espessura);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
