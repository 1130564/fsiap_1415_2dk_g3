/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.hclc;

import fsiap2dkg3.model.Dissipador;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Emanuel Marques
 */
public class RegistoDissipadoresTest {

    public RegistoDissipadoresTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class RegistoDissipadores.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        RegistoDissipadores outro = new RegistoDissipadores();
        RegistoDissipadores instance = new RegistoDissipadores();
        boolean expResult = true;
        boolean result = instance.equals(outro);
        assertEquals(expResult, result);
    }

    /**
     * Test of addDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testAddDissipador() {
        System.out.println("addDissipador");
        Dissipador o = new Dissipador("teste", "teste", 1, 1);
        RegistoDissipadores instance = new RegistoDissipadores();
        boolean expResult = true;
        boolean result = instance.addDissipador(o);
        assertEquals(expResult, result);

    }

    /**
     * Test of rmDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testRmDissipador_Dissipador() {
        System.out.println("rmDissipador");
        Dissipador o = new Dissipador("teste", "teste", 1, 1);
        RegistoDissipadores instance = new RegistoDissipadores();
        instance.addDissipador(o);
        boolean expResult = true;
        boolean result = instance.rmDissipador(o);
        assertEquals(expResult, result);

    }

    /**
     * Test of rmDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testRmDissipador_int() {
        System.out.println("rmDissipador");
        int pos = 0;
        Dissipador o = new Dissipador("teste", "teste", 1, 1);
        RegistoDissipadores instance = new RegistoDissipadores();
        instance.addDissipador(o);
       
        Dissipador result = instance.rmDissipador(pos);
        assertEquals(o, result);

    }

    /**
     * Test of replaceDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testReplaceDissipador() {
        System.out.println("replaceDissipador");
        Dissipador old = new Dissipador("teste", "teste", 1, 1);
        Dissipador novo = new Dissipador("teste2", "teste2", 1, 1);
        RegistoDissipadores instance = new RegistoDissipadores();
        instance.addDissipador(old);
        boolean expResult = true;
        boolean result = instance.replaceDissipador(old, novo);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of editDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testEditDissipador_5args_1() {
        System.out.println("editDissipador");
        int pos = 0;
        String nome = "teste3";
        String tipo = "teste3";
        float condutividadeTermica = 2.0F;
        float espessura = 2.0F;
         Dissipador o = new Dissipador("teste", "teste", 1, 1);
        RegistoDissipadores instance = new RegistoDissipadores();
        instance.addDissipador(o);
        boolean expResult = true;
        boolean result = instance.editDissipador(pos, nome, tipo, condutividadeTermica, espessura);
        assertEquals(expResult, result);
     
    }

    /**
     * Test of editDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testEditDissipador_5args_2() {
        System.out.println("editDissipador");
        Dissipador o = new Dissipador("teste", "teste", 1, 1);
        String nome = "teste3";
        String tipo = "teste3";
        float condutividadeTermica = 2.0F;
        float espessura = 2.0F;     
        RegistoDissipadores instance = new RegistoDissipadores();
        instance.addDissipador(o);
        boolean expResult = true;
        boolean result = instance.editDissipador(o, nome, tipo, condutividadeTermica, espessura);
        assertEquals(expResult, result);
  
    }



    /**
     * Test of indexOfDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testIndexOfDissipador() {
        System.out.println("indexOfDissipador");
        Dissipador o = new Dissipador("teste", "teste", 1, 1);
        RegistoDissipadores instance = new RegistoDissipadores();
        instance.addDissipador(o);
        int expResult = 0;
        int result = instance.indexOfDissipador(o);
        assertEquals(expResult, result);
   
    }

    /**
     * Test of contemDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testContemDissipador() {
        System.out.println("contemDissipador");
        Dissipador o = new Dissipador("teste", "teste", 1, 1);
        RegistoDissipadores instance = new RegistoDissipadores();
        instance.addDissipador(o);
        boolean expResult = true;
        boolean result = instance.contemDissipador(o);
        assertEquals(expResult, result);
   
    }

    /**
     * Test of validaDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testValidaDissipador() {
        System.out.println("validaDissipador");
        Dissipador o = new Dissipador("teste", "teste", 1, 1);
        RegistoDissipadores instance = new RegistoDissipadores();
        boolean expResult = true;
        boolean result = instance.validaDissipador(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of novoDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testNovoDissipador_0args() {
        System.out.println("novoDissipador");
        Dissipador expResult = new Dissipador();
        Dissipador result = RegistoDissipadores.novoDissipador();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of novoDissipador method, of class RegistoDissipadores.
     */
    @Test
    public void testNovoDissipador_4args() {
        System.out.println("novoDissipador");
        String nome = "teste";
        String tipo = "teste";
        float condutividadeTermica = 2.0F;
        float espessura = 2.0F;
        Dissipador expResult = new Dissipador("teste", "teste", 2, 2);
        Dissipador result = RegistoDissipadores.novoDissipador(nome, tipo, condutividadeTermica, espessura);
        assertEquals(expResult, result);
    }

}
