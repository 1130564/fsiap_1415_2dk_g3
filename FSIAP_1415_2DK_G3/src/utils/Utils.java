package utils;

import fsiap2dkg3.VARS;
import fsiap2dkg3.dicionario.Dicionario;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 * Utilidades
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 */
public class Utils {
    private Utils(){
    }
    /**
     * Confirmação
     * @return TRUE caso sejá confirmado. Caso Contrário retorna FALSE.
     */
    public static boolean confirmar(){
         String[] op = {Dicionario.getSignificado("Nao"),Dicionario.getSignificado("Sim")};
            int resposta = JOptionPane.showOptionDialog(null,
                    Dicionario.getSignificado("Continuar")+"\n"
                    + "("+Dicionario.getSignificado("IrreversivelProcesso")+")",
                    Dicionario.getSignificado("Confirmacao"),
                    0,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    op,
                    op[0]);
            return (resposta == 1);
    }
    
    /**
     * Tipos de ChooserDialogs (OpenDialog, SaveDialog)
     */
    public static enum TypeChooser {OPEN,SAVE,ALL};
    /**
     * Escolher um Ficheiro
     * [REF]http://www.java2s.com/Tutorial/Java/0240__Swing/1260__JFileChooser.htm
     *
     * @param parent Pai da ChooserDialog
     * @param title Título da ChooserDialog
     * @param buttonText Texto do Botão
     * @param selectedFile Ficheiro Selecionado Pré-Definido, caso seja null é Selecionado o ultimo ficheiro Selecionado
     * @param type Tipo de ChooserDialog
     * @return Ficheiro Selecionado. Caso contrario, retorna null.
     */
    
    public static File fileChooser(Component parent, String title, String buttonText, File selectedFile,TypeChooser type) {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle(title);
        chooser.setSelectedFile((selectedFile != null) ? selectedFile : VARS.LASTFILE);
        chooser.setApproveButtonText(buttonText);
        int op;
        if(type==TypeChooser.OPEN){
             op = chooser.showOpenDialog(parent);
        }else{
             op = chooser.showDialog(parent,buttonText);
        }
        if (op == JFileChooser.APPROVE_OPTION) {
            VARS.LASTFILE = chooser.getSelectedFile();
            VARS.ALTERACOES=true;
            return chooser.getSelectedFile();
        }
        return null;
    }
    
    
    /**
     * Extensão do Ficheiro ex:(file.txt>txt)
     * @param file Ficheiro
     * @return Extensão do Ficheiro 
     */
    public static String getExtensao(File file) {
        String ext = null;
        String nome = file.getName();
        int indexPonto = nome.lastIndexOf('.');
        if (indexPonto >= 0) {
            ext = nome.substring(indexPonto + 1).toLowerCase();
        }
        return ext;
    }
    
        /**
     * Converter Caminho Absoluto para Caminho Relativo
     * [REF]http://stackoverflow.com/questions/204784/how-to-construct-a-relative-path-in-java-from-two-absolute-paths-or-urls
     *
     * @param base Ponto de Partida
     * @param absolute Caminho Absoluto
     * @return Caminho Relativo
     */
    public static String pathAbsoluteToRelative(String base, String absolute) {
        Path pathAbsolute = Paths.get(absolute);
        Path pathBase = Paths.get(base);
        Path pathRelative = pathBase.relativize(pathAbsolute);
        return pathRelative.toString();
    }

    /**
     * Converter Absolute Path para Caminho Relativo
     * [REF]http://stackoverflow.com/questions/204784/how-to-construct-a-relative-path-in-java-from-two-absolute-paths-or-urls
     *
     * @param absolute Caminho Absoluto
     * @return Caminho Relativo
     */
    public static String pathAbsoluteToRelative(String absolute) {
        return pathAbsoluteToRelative(new File("").getAbsolutePath(), absolute);
    }

    /**
     * Converter Caminho Relativo para Caminho Absoluto
     * [REF]http://stackoverflow.com/questions/3204955/converting-relative-paths-to-absolute-paths
     *
     * @param base Ponto de Partida
     * @param relative Caminho Relativo
     * @return Caminho Absoluto
     * @throws IOException Caminho Inválido
     */
    public static String pathRelativeToAbsolute(String base, String relative) throws IOException {
        if (base != null) {
            return new File(base, relative).getCanonicalFile().toString();
        } else {
            return new File(relative).getCanonicalFile().toString();
        }
    }

    /**
     * Converter Caminho Relativo para Caminho Absoluto
     *
     * @param relative Caminho Relativo
     * @return Caminho Absoluto
     * @throws IOException Caminho Inválido
     */
    public static String pathRelativeToAbsolute(String relative) throws IOException {
        return pathRelativeToAbsolute(null, relative);
    }
}
