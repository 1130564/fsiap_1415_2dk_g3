package utils;

/**
 * Item, utilizado para escolhas entre opções
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 */
public class Item {

    // Variáveis de Instância \\
    /**
     * Label do Item
     */
    private String label;

    /**
     * ID do Item
     */
    private int id;

    /**
     * Objeto do Item
     */
    private Object object;

    // Construtores \\
    /**
     * Constrói uma instância de item recebendo por parâmetro o ID, label e o
     * object
     *
     * @param id ID do Item
     * @param label Label do Item
     * @param object Objecto do Item
     */
    public Item(int id, String label, Object object) {
        this.setID(id);
        this.setLabel(label);
        this.setObject(object);
    }

    /**
     * Constrói uma instância de item recebendo por parâmetro o id e o label
     *
     * @param id ID do Item
     * @param label Label do Item
     */
    public Item(int id, String label) {
        this.setID(id);
        this.setLabel(label);
    }

    /**
     * Consultar ID do Item
     *
     * @return ID
     */
    public int getID() {
        return id;
    }

    /**
     * Consultar Label do Item
     *
     * @return Label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Consultar Objeto do Item
     *
     * @return Objecto
     */
    public Object getObject() {
        return this.object;
    }

    /**
     * Alterar ID do Item
     *
     * @param id Novo ID
     */
    public final void setID(int id) {
        this.id = id;
    }

    /**
     * Alterar Label do Item
     *
     * @param label Novo Label
     */
    public final void setLabel(String label) {
        this.label = label;
    }

    /**
     * Alterar Object do Item
     *
     * @param object Novo Objecto
     */
    public final void setObject(Object object) {
        this.object = object;
    }

    /**
     * Representação Textual da Instância Formatada
     *
     * @return Representação Textual do Item
     */
    @Override
    public String toString() {
        return this.label;
    }

    /**
     * Compara o Item com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Item equivalente ao
     * Item. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        Item i = (Item) outro;
        return this.id == i.getID();
    }
}
