package fsiap2dkg3;


import fsiap2dkg3.gui.MenuUI;
//import javax.swing.UIManager;

/**
 * Execução da Aplicação
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class MAIN {

    /**
     * Fluxo Principal de Execução da Aplicação
     *
     * @param args Argumentos da Linha de Comandos
     */
    public static void main(String[] args) {
        try {
            //[REF]http://docs.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
            //ESTILO DE JANELA
        //   UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
        }
        //[REF]http://javarevisited.blogspot.pt/2011/09/invokeandwait-invokelater-swing-example.html
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MenuUI menu = new MenuUI();
            }
        });

    }

}
