package fsiap2dkg3.gui;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.EditarProcessadorController;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.model.Processador;
import utils.Utils;
import javax.swing.JOptionPane;

/**
 * Editar processador
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class DialogEditarProcessador extends javax.swing.JDialog {

    /**
     * Valor de Retorno da Dialog
     */
    private Object information = false;

    /**
     * Controller
     */
    private EditarProcessadorController controller;

    /**
     * Construir Dailog
     *
     * @param parent Pai Dialog
     * @param modal Modal Mode
     * @param controller Controller
     */
    public DialogEditarProcessador(java.awt.Frame parent, boolean modal, EditarProcessadorController controller) {
        super(parent, modal);
        this.controller = controller;
        initComponents();
        updateValues();
        setVisible(true);
    }

    /**
     * Actualizar Valores dos Componentes
     */
    public final void updateValues() {
        Processador processador = this.controller.getProcessador();
        jtfNome.setText(processador.getNome());
        jftfPotenciaDissipada.setValue(processador.getPotenciaDissipada());
        jftfTemperaturaMaxima.setValue(processador.getMaxTemperatura());
        jftfArea.setValue(processador.getArea());
    }

    /**
     * Valor de Retorno da Daiog
     *
     * @return Sucesso da Operação(FALSE=Operação não foi Concluída com Sucesso)
     */
    public Object getInformation() {
        return this.information;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlNome = new javax.swing.JLabel();
        jlPotenciaDissipada = new javax.swing.JLabel();
        jlTemperaturaMaxima = new javax.swing.JLabel();
        jlArea = new javax.swing.JLabel();
        jtfNome = new javax.swing.JTextField();
        jftfPotenciaDissipada = new javax.swing.JFormattedTextField();
        jftfTemperaturaMaxima = new javax.swing.JFormattedTextField();
        jftfArea = new javax.swing.JFormattedTextField();
        jlUnidadePotenciaDissipada = new javax.swing.JLabel();
        jlUnidadeTemperaturaMaxima = new javax.swing.JLabel();
        jlUnidadeArea = new javax.swing.JLabel();
        jbAdicionar = new javax.swing.JButton();
        jbCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("fsiap2dkg3/dicionario/AdicionarProcessador"); // NOI18N
        setTitle(bundle.getString("DialogAdicionarProcessador.title")); // NOI18N
        setResizable(false);

        jlNome.setText(bundle.getString("DialogAdicionarProcessador.jlNome.text")); // NOI18N

        jlPotenciaDissipada.setText(bundle.getString("DialogAdicionarProcessador.jlPotenciaDissipada.text")); // NOI18N

        jlTemperaturaMaxima.setText(bundle.getString("DialogAdicionarProcessador.jlTemperaturaMaxima.text")); // NOI18N

        jlArea.setText(bundle.getString("DialogAdicionarProcessador.jlArea.text")); // NOI18N

        jftfPotenciaDissipada.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        jftfTemperaturaMaxima.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        jftfArea.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        jlUnidadePotenciaDissipada.setText(bundle.getString("DialogAdicionarProcessador.jlUnidadePotenciaDissipada.text")); // NOI18N

        jlUnidadeTemperaturaMaxima.setText(bundle.getString("DialogAdicionarProcessador.jlUnidadeTemperaturaMaxima.text")); // NOI18N

        jlUnidadeArea.setText(bundle.getString("DialogAdicionarProcessador.jlUnidadeArea.text")); // NOI18N

        jbAdicionar.setText(bundle.getString("DialogAdicionarProcessador.jbAdicionar.text")); // NOI18N
        jbAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbAdicionarActionPerformed(evt);
            }
        });

        jbCancelar.setText(bundle.getString("DialogAdicionarProcessador.jbCancelar.text")); // NOI18N
        jbCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jlArea)
                            .addComponent(jlPotenciaDissipada)
                            .addComponent(jlNome)
                            .addComponent(jlTemperaturaMaxima))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jftfArea)
                            .addComponent(jftfPotenciaDissipada)
                            .addComponent(jftfTemperaturaMaxima)
                            .addComponent(jtfNome, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlUnidadePotenciaDissipada)
                            .addComponent(jlUnidadeTemperaturaMaxima)
                            .addComponent(jlUnidadeArea)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(jbAdicionar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbCancelar)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlNome)
                    .addComponent(jtfNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlPotenciaDissipada)
                    .addComponent(jftfPotenciaDissipada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlUnidadePotenciaDissipada))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlTemperaturaMaxima)
                    .addComponent(jftfTemperaturaMaxima, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlUnidadeTemperaturaMaxima))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlArea)
                    .addComponent(jftfArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlUnidadeArea))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbAdicionar)
                    .addComponent(jbCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Cancelar Operação
     *
     * @param evt
     */
    private void jbCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCancelarActionPerformed
        this.information = false;
        dispose();
    }//GEN-LAST:event_jbCancelarActionPerformed

    /**
     * Concluir Operação
     *
     * @param evt
     */
    private void jbAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbAdicionarActionPerformed
        try {
            if (jtfNome.getText().equals("")
                    || jftfPotenciaDissipada.getValue() == null
                    || jftfTemperaturaMaxima.getValue() == null
                    || jftfArea.getValue() == null) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado("Erro")+": "+Dicionario.getSignificado("DadosInvalidos")+"!", getTitle(), JOptionPane.WARNING_MESSAGE);
                return;
            }
            if (!Utils.confirmar()) {
                return;
            }

            String nome = jtfNome.getText();
            float potenciaDissipada = Float.parseFloat(jftfPotenciaDissipada.getValue().toString());
            float temperaturaMaxima = Float.parseFloat(jftfTemperaturaMaxima.getValue().toString());
            float area = Float.parseFloat(jftfArea.getValue().toString());

            if (this.controller.editProcessador(nome, potenciaDissipada, temperaturaMaxima, area)) {
                this.information = true;
                dispose();
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado("Erro")+": "+Dicionario.getSignificado("DadosInvalidos")+"!", getTitle(), JOptionPane.WARNING_MESSAGE);
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado("Erro")+": " + ex + "!", getTitle(), JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jbAdicionarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jbAdicionar;
    private javax.swing.JButton jbCancelar;
    private javax.swing.JFormattedTextField jftfArea;
    private javax.swing.JFormattedTextField jftfPotenciaDissipada;
    private javax.swing.JFormattedTextField jftfTemperaturaMaxima;
    private javax.swing.JLabel jlArea;
    private javax.swing.JLabel jlNome;
    private javax.swing.JLabel jlPotenciaDissipada;
    private javax.swing.JLabel jlTemperaturaMaxima;
    private javax.swing.JLabel jlUnidadeArea;
    private javax.swing.JLabel jlUnidadePotenciaDissipada;
    private javax.swing.JLabel jlUnidadeTemperaturaMaxima;
    private javax.swing.JTextField jtfNome;
    // End of variables declaration//GEN-END:variables
}
