package fsiap2dkg3.gui;

import fsiap2dkg3.VARS;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.dicionario.Idioma;
import fsiap2dkg3.model.*;
import fsiap2dkg3.ui.*;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import utils.Item;
import fsiap2dkg3.model.Calculo;

public class MenuUI extends javax.swing.JFrame {

    private int tamanhoAtualizado;
    boolean flagTam;

    public MenuUI() {
        VARS.CURRENTFRAME = MenuUI.this;
        InicioRunUI ui = new InicioRunUI();
        boolean returnRun = ui.run();
        initComponents();
        updateValues();
        tamanhoAtualizado = this.getHeight() + 80;
        flagTam = false;
        btnIcon.setVisible(false);
        setVisible(true);
    }

    public final void updateValues() {
        updateMenuIdiomas();
        updateLang();
        updateCombos();
    }

    private void updateMenuIdiomas() {
        for (Idioma i : Dicionario.IDIOMAS) {
            javax.swing.JMenuItem jmiNovoIdioma = new javax.swing.JMenuItem();
            jmiNovoIdioma.setText(i.getIdioma());
            jmiNovoIdioma.setToolTipText(i.getTag());
            jmiNovoIdioma.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jmiTrocarIdioma(evt);
                }

            });
            jmIdioma.add(jmiNovoIdioma);
        }
    }

    private void updateLang() {
        jmiBackupCarregar.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiBackupCarregar.text"));
        jlTemperaturaAmbiente.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jlTemperaturaAmbiente.text"));
        jlDissipador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jlDissipador.text"));
        jmDissipador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmDissipador.text"));
        jmiAdicionarDissipador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiAdicionarDissipador.text"));
        jmiRemoverPTermica.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiRemoverPTermica.text"));
        jlUnidadeTemperaturaAmbiente.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jlUnidadeTemperaturaAmbiente.text"));
        jlProcessador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jlProcessador.text"));
        jlPastaTermica.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jlPastaTermica.text"));
        jmiEditarPTermica.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiEditarPTermica.text"));
        jmiAdicionarPTermica.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiAdicionarPTermica.text"));
        jmPastaTermica.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmPastaTermica.text"));
        jmiRemoverProcessador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiRemoverProcessador.text"));
        jmiEditarProcessador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiEditarProcessador.text"));
        jmiAdicionarProcessador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiAdicionarProcessador.text"));
        jmProcessador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmProcessador.text"));
        jmiSair.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiSair.text"));
        jmiBackupSair.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiBackupSair.text"));
        jbAnalisar.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jbAnalisar.text"));
        jmiAjudaSobre.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiAjudaSobre.text"));
        jmiRemoverDissipador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiRemoverDissipador.text"));
        jmFicheiro.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmFicheiro.text"));
        jmiEditarDissipador.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiEditarDissipador.text"));
        jmBackup.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmBackup.text"));
        jmiAjudaManual.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmiAjudaManual.text"));
        jmAjuda.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmAjuda.text"));
        jmIdioma.setText(Dicionario.getSignificado(Dicionario.PATH + "MenuUI", "MenuUI.jmIdioma.text"));
    }

    private void updateCombos() {
        updateComboProcessadores();
        jcbProcessador.setSelectedIndex(-1);
        updateComboPastaTermica();
        jcbPastaTermica.setSelectedIndex(-1);
        updateComboDissipadores();
        jcbDissipador.setSelectedIndex(-1);
    }

    private void updateComboProcessadores() {
        jcbProcessador.removeAllItems();
        for (Processador p : VARS.SISTEMA.getRegistoProcessadores().getProcessadores()) {
            jcbProcessador.addItem(new Item(jcbProcessador.getItemCount() + 1, p.getNome(), p));
        }
    }

    private void updateComboPastaTermica() {
        jcbPastaTermica.removeAllItems();
        for (PastaTermica p : VARS.SISTEMA.getRegistoPastasTermicas().getPastasTermicas()) {
            jcbPastaTermica.addItem(new Item(jcbPastaTermica.getItemCount() + 1, p.getNome(), p));
        }
    }

    private void updateComboDissipadores() {
        jcbDissipador.removeAllItems();
        for (Dissipador p : VARS.SISTEMA.getRegistoDissipadores().getDissipadores()) {
            jcbDissipador.addItem(new Item(jcbDissipador.getItemCount() + 1, p.getNome(), p));
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlProcessador = new javax.swing.JLabel();
        jlPastaTermica = new javax.swing.JLabel();
        jlDissipador = new javax.swing.JLabel();
        jcbProcessador = new javax.swing.JComboBox();
        jcbPastaTermica = new javax.swing.JComboBox();
        jcbDissipador = new javax.swing.JComboBox();
        jlTemperaturaAmbiente = new javax.swing.JLabel();
        jpTemperaturaAmbiente = new javax.swing.JSpinner();
        jlUnidadeTemperaturaAmbiente = new javax.swing.JLabel();
        jbAnalisar = new javax.swing.JButton();
        lblResposta = new javax.swing.JLabel();
        btnIcon = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jmFicheiro = new javax.swing.JMenu();
        jmIdioma = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jmBackup = new javax.swing.JMenu();
        jmiBackupCarregar = new javax.swing.JMenuItem();
        jmiBackupSair = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jmiSair = new javax.swing.JMenuItem();
        jmProcessador = new javax.swing.JMenu();
        jmiAdicionarProcessador = new javax.swing.JMenuItem();
        jmiEditarProcessador = new javax.swing.JMenuItem();
        jmiRemoverProcessador = new javax.swing.JMenuItem();
        jmPastaTermica = new javax.swing.JMenu();
        jmiAdicionarPTermica = new javax.swing.JMenuItem();
        jmiEditarPTermica = new javax.swing.JMenuItem();
        jmiRemoverPTermica = new javax.swing.JMenuItem();
        jmDissipador = new javax.swing.JMenu();
        jmiAdicionarDissipador = new javax.swing.JMenuItem();
        jmiEditarDissipador = new javax.swing.JMenuItem();
        jmiRemoverDissipador = new javax.swing.JMenuItem();
        jmAjuda = new javax.swing.JMenu();
        jmiAjudaManual = new javax.swing.JMenuItem();
        jmiAjudaSobre = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("fsiap2dkg3/dicionario/MenuUI"); // NOI18N
        jlProcessador.setText(bundle.getString("MenuUI.jlProcessador.text")); // NOI18N

        jlPastaTermica.setText(bundle.getString("MenuUI.jlPastaTermica.text")); // NOI18N

        jlDissipador.setText(bundle.getString("MenuUI.jlDissipador.text")); // NOI18N

        jcbProcessador.setMaximumSize(new java.awt.Dimension(96, 27));

        jcbPastaTermica.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        jcbPastaTermica.setMaximumSize(new java.awt.Dimension(96, 27));

        jcbDissipador.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        jcbDissipador.setMaximumSize(new java.awt.Dimension(96, 27));

        jlTemperaturaAmbiente.setText(bundle.getString("MenuUI.jlTemperaturaAmbiente.text")); // NOI18N

        jpTemperaturaAmbiente.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.0f), null, null, Float.valueOf(1.0f)));

        jlUnidadeTemperaturaAmbiente.setText(bundle.getString("MenuUI.jlUnidadeTemperaturaAmbiente.text")); // NOI18N

        jbAnalisar.setText(bundle.getString("MenuUI.jbAnalisar.text")); // NOI18N
        jbAnalisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbAnalisarActionPerformed(evt);
            }
        });

        lblResposta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jmFicheiro.setText(bundle.getString("MenuUI.jmFicheiro.text")); // NOI18N

        jmIdioma.setText(bundle.getString("MenuUI.jmIdioma.text")); // NOI18N
        jmFicheiro.add(jmIdioma);
        jmFicheiro.add(jSeparator1);

        jmBackup.setText(bundle.getString("MenuUI.jmBackup.text")); // NOI18N

        jmiBackupCarregar.setText(bundle.getString("MenuUI.jmiBackupCarregar.text")); // NOI18N
        jmiBackupCarregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiBackupCarregarActionPerformed(evt);
            }
        });
        jmBackup.add(jmiBackupCarregar);

        jmiBackupSair.setText(bundle.getString("MenuUI.jmiBackupSair.text")); // NOI18N
        jmiBackupSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiBackupSairActionPerformed(evt);
            }
        });
        jmBackup.add(jmiBackupSair);

        jmFicheiro.add(jmBackup);
        jmFicheiro.add(jSeparator2);

        jmiSair.setText(bundle.getString("MenuUI.jmiSair.text")); // NOI18N
        jmiSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSairActionPerformed(evt);
            }
        });
        jmFicheiro.add(jmiSair);

        jMenuBar1.add(jmFicheiro);

        jmProcessador.setText(bundle.getString("MenuUI.jmProcessador.text")); // NOI18N

        jmiAdicionarProcessador.setText(bundle.getString("MenuUI.jmiAdicionarProcessador.text")); // NOI18N
        jmiAdicionarProcessador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiAdicionarProcessadorActionPerformed(evt);
            }
        });
        jmProcessador.add(jmiAdicionarProcessador);

        jmiEditarProcessador.setText(bundle.getString("MenuUI.jmiEditarProcessador.text")); // NOI18N
        jmiEditarProcessador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiEditarProcessadorActionPerformed(evt);
            }
        });
        jmProcessador.add(jmiEditarProcessador);

        jmiRemoverProcessador.setText(bundle.getString("MenuUI.jmiRemoverProcessador.text")); // NOI18N
        jmiRemoverProcessador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiRemoverProcessadorActionPerformed(evt);
            }
        });
        jmProcessador.add(jmiRemoverProcessador);

        jMenuBar1.add(jmProcessador);

        jmPastaTermica.setText(bundle.getString("MenuUI.jmPastaTermica.text")); // NOI18N

        jmiAdicionarPTermica.setText(bundle.getString("MenuUI.jmiAdicionarPTermica.text")); // NOI18N
        jmiAdicionarPTermica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiAdicionarPTermicaActionPerformed(evt);
            }
        });
        jmPastaTermica.add(jmiAdicionarPTermica);

        jmiEditarPTermica.setText(bundle.getString("MenuUI.jmiEditarPTermica.text")); // NOI18N
        jmiEditarPTermica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiEditarPTermicaActionPerformed(evt);
            }
        });
        jmPastaTermica.add(jmiEditarPTermica);

        jmiRemoverPTermica.setText(bundle.getString("MenuUI.jmiRemoverPTermica.text")); // NOI18N
        jmiRemoverPTermica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiRemoverPTermicaActionPerformed(evt);
            }
        });
        jmPastaTermica.add(jmiRemoverPTermica);

        jMenuBar1.add(jmPastaTermica);

        jmDissipador.setText(bundle.getString("MenuUI.jmDissipador.text")); // NOI18N

        jmiAdicionarDissipador.setText(bundle.getString("MenuUI.jmiAdicionarDissipador.text")); // NOI18N
        jmiAdicionarDissipador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiAdicionarDissipadorActionPerformed(evt);
            }
        });
        jmDissipador.add(jmiAdicionarDissipador);

        jmiEditarDissipador.setText(bundle.getString("MenuUI.jmiEditarDissipador.text")); // NOI18N
        jmiEditarDissipador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiEditarDissipadorActionPerformed(evt);
            }
        });
        jmDissipador.add(jmiEditarDissipador);

        jmiRemoverDissipador.setText(bundle.getString("MenuUI.jmiRemoverDissipador.text")); // NOI18N
        jmiRemoverDissipador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiRemoverDissipadorActionPerformed(evt);
            }
        });
        jmDissipador.add(jmiRemoverDissipador);

        jMenuBar1.add(jmDissipador);

        jmAjuda.setText(bundle.getString("MenuUI.jmAjuda.text")); // NOI18N

        jmiAjudaManual.setText(bundle.getString("MenuUI.jmiAjudaManual.text")); // NOI18N
        jmiAjudaManual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiAjudaManualActionPerformed(evt);
            }
        });
        jmAjuda.add(jmiAjudaManual);

        jmiAjudaSobre.setText(bundle.getString("MenuUI.jmiAjudaSobre.text")); // NOI18N
        jmiAjudaSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiAjudaSobreActionPerformed(evt);
            }
        });
        jmAjuda.add(jmiAjudaSobre);

        jMenuBar1.add(jmAjuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(84, Short.MAX_VALUE)
                .addComponent(jlTemperaturaAmbiente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jpTemperaturaAmbiente, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlUnidadeTemperaturaAmbiente)
                .addGap(79, 79, 79))
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblResposta, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(151, 151, 151)
                                .addComponent(jbAnalisar))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jlProcessador)
                                    .addComponent(jlPastaTermica)
                                    .addComponent(jlDissipador))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jcbPastaTermica, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jcbDissipador, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jcbProcessador, 0, 253, Short.MAX_VALUE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlProcessador)
                    .addComponent(jcbProcessador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlPastaTermica)
                    .addComponent(jcbPastaTermica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbDissipador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlDissipador))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlTemperaturaAmbiente)
                    .addComponent(jlUnidadeTemperaturaAmbiente)
                    .addComponent(jpTemperaturaAmbiente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbAnalisar)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblResposta, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIcon))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jmiTrocarIdioma(java.awt.event.ActionEvent evt) {
        Dicionario.setLANG(((javax.swing.JMenuItem) evt.getSource()).getToolTipText());
        updateLang();
    }

    private void jmiAdicionarProcessadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiAdicionarProcessadorActionPerformed
        AdicionarProcessadorUI ui = new AdicionarProcessadorUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            updateComboProcessadores();
        }
        VARS.ALTERACOES = (VARS.ALTERACOES == true) ? true : returnRun;
    }//GEN-LAST:event_jmiAdicionarProcessadorActionPerformed

    private void jmiEditarProcessadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiEditarProcessadorActionPerformed
        EditarProcessadorUI ui = new EditarProcessadorUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            updateComboProcessadores();
        }
        VARS.ALTERACOES = (VARS.ALTERACOES == true) ? true : returnRun;
    }//GEN-LAST:event_jmiEditarProcessadorActionPerformed

    private void jmiRemoverProcessadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiRemoverProcessadorActionPerformed
        RemoverProcessadorUI ui = new RemoverProcessadorUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            updateComboProcessadores();
        }
        VARS.ALTERACOES = (VARS.ALTERACOES == true) ? true : returnRun;
    }//GEN-LAST:event_jmiRemoverProcessadorActionPerformed

    private void jmiAjudaSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiAjudaSobreActionPerformed
        JOptionPane.showMessageDialog(VARS.CURRENTFRAME,
                "Instituto Superior de Engenharia do Porto"
                + "\nLicenciatura em Engenharia Informática\n"
                + "Física Aplicada"
                + "\n\nEmanuel Marques - 1130553@isep.ipp.pt\nVitor Moreira - 1130564@isep.ipp.pt\n"
                + "Gilberto Pereira - 1131251@isep.ipp.pt\nPaulo Andrade - 1130313@isep.ipp.pt"
                + "\n\nCopyright © 2014/2015 2DK-G3. All rights reserved.", "Sobre", JOptionPane.INFORMATION_MESSAGE);

    }//GEN-LAST:event_jmiAjudaSobreActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        jmiSairActionPerformed(null);
    }//GEN-LAST:event_formWindowClosing

    private void jmiSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSairActionPerformed
        FimRunUI ui = new FimRunUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            dispose();
        }
    }//GEN-LAST:event_jmiSairActionPerformed

    private void jbAnalisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbAnalisarActionPerformed

        if (this.jcbProcessador.getSelectedItem() != null
                && this.jcbPastaTermica.getSelectedItem() != null
                && this.jcbDissipador.getSelectedItem() != null
                && (float) this.jpTemperaturaAmbiente.getValue() > 0) {

            Item iP = (Item) this.jcbProcessador.getSelectedItem();
            Processador p = (Processador) iP.getObject();

            Item iPt = (Item) this.jcbPastaTermica.getSelectedItem();
            PastaTermica pt = (PastaTermica) iPt.getObject();

            Item iD = (Item) this.jcbDissipador.getSelectedItem();
            Dissipador d = (Dissipador) iD.getObject();

            float aux = (float) this.jpTemperaturaAmbiente.getValue();
            int tempMaquina = (int) aux;

            if (!flagTam) {
                this.setSize(this.getWidth(), this.getHeight() + 40);
                flagTam = true;
            }

            if (Calculo.calcTemp(p, pt, d, tempMaquina) < p.getMaxTemperatura()) {

                this.lblResposta.setText("Valido!");
                Image img;
                try {
                    img = ImageIO.read(new File("icon_sucess.png"));
                    btnIcon.setIcon(new ImageIcon(img));
                    btnIcon.setBorderPainted(false);
                    btnIcon.setContentAreaFilled(false);
                    btnIcon.setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(MenuUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                this.lblResposta.setText("Invalido!");
                Image img;
                try {
                    img = ImageIO.read(new File("icon_fail.png"));
                    btnIcon.setIcon(new ImageIcon(img));
                    btnIcon.setBorderPainted(false);
                    btnIcon.setContentAreaFilled(false);
                    btnIcon.setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(MenuUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {

            JOptionPane.showMessageDialog(VARS.CURRENTFRAME,
                    "Tem de selecionar os componentes primeiro!",
                    "Erro!",
                    JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jbAnalisarActionPerformed

    private void jmiAdicionarDissipadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiAdicionarDissipadorActionPerformed
        AdicionarDissipadorUI ui = new AdicionarDissipadorUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            updateComboDissipadores();
        }
        VARS.ALTERACOES = (VARS.ALTERACOES == true) ? true : returnRun;    }//GEN-LAST:event_jmiAdicionarDissipadorActionPerformed

    private void jmiEditarDissipadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiEditarDissipadorActionPerformed
        EditarDissipadorUI ui = new EditarDissipadorUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            updateComboDissipadores();
        }
        VARS.ALTERACOES = (VARS.ALTERACOES == true) ? true : returnRun;    }//GEN-LAST:event_jmiEditarDissipadorActionPerformed

    private void jmiRemoverDissipadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiRemoverDissipadorActionPerformed
        RemoverDissipadorUI ui = new RemoverDissipadorUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            updateComboDissipadores();
        }
        VARS.ALTERACOES = (VARS.ALTERACOES == true) ? true : returnRun;    }//GEN-LAST:event_jmiRemoverDissipadorActionPerformed

    private void jmiAjudaManualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiAjudaManualActionPerformed
        JOptionPane.showMessageDialog(VARS.CURRENTFRAME,
                "Esta aplicação, dado um processador, a sua \n"
                + "temperatura máxima, a temperatura ambiente e depois \n"
                + "a sua pasta térmica e dissipador,\n"
                + " testa essa combinação e mostra se a"
                + " combinação resulta ou não, isto é, se a temperatura\n"
                + " máxima do processador não é excedida, prevenindo \n"
                + "assim a danificação de componentes e assegurando \n"
                + "um bom funcionamento do sistema."
                + "\n\n"
                + "Os menus de topo servem para gerir os componentes \n"
                + "existentes no sistema, permintindo, adicionar, editar e remover \n"
                + "cada um deles. No menu \"Ficheiro\" é possível \n"
                + "alterar o idioma do sistema.\n\n"
                + "Para efetuar o cálculo, é necessario escolher \n"
                + "os componentes e fornecer a temperatura \n"
                + "ambiente, para depois \"clickar\" em \"Analisar\" para \n"
                + "efetuar o cálculo.", "Manual da aplicação", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jmiAjudaManualActionPerformed

    private void jmiAdicionarPTermicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiAdicionarPTermicaActionPerformed
        // TODO add your handling code here:
        AdicionarPastaTermicaUI ui = new AdicionarPastaTermicaUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            updateComboPastaTermica();
        }
        VARS.ALTERACOES = (VARS.ALTERACOES == true) ? true : returnRun;
    }//GEN-LAST:event_jmiAdicionarPTermicaActionPerformed

    private void jmiEditarPTermicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiEditarPTermicaActionPerformed
        // TODO add your handling code here:
        EditarPastaTermicaUI ui = new EditarPastaTermicaUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            updateComboPastaTermica();
        }
        VARS.ALTERACOES = (VARS.ALTERACOES == true) ? true : returnRun;
    }//GEN-LAST:event_jmiEditarPTermicaActionPerformed

    private void jmiRemoverPTermicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiRemoverPTermicaActionPerformed
        // TODO add your handling code here:
        RemoverPastaTermicaUI ui = new RemoverPastaTermicaUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            updateComboPastaTermica();
        }
        VARS.ALTERACOES = (VARS.ALTERACOES == true) ? true : returnRun;
    }//GEN-LAST:event_jmiRemoverPTermicaActionPerformed

    private void jmiBackupCarregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiBackupCarregarActionPerformed
        BackupCarregarUI ui = new BackupCarregarUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            VARS.ALTERACOES = false;
        }
    }//GEN-LAST:event_jmiBackupCarregarActionPerformed

    private void jmiBackupSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiBackupSairActionPerformed
        BackupGuardarUI ui = new BackupGuardarUI();
        boolean returnRun = ui.run();
        if (returnRun) {
            VARS.ALTERACOES = false;
        }
    }//GEN-LAST:event_jmiBackupSairActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIcon;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JButton jbAnalisar;
    private javax.swing.JComboBox jcbDissipador;
    private javax.swing.JComboBox jcbPastaTermica;
    private javax.swing.JComboBox jcbProcessador;
    private javax.swing.JLabel jlDissipador;
    private javax.swing.JLabel jlPastaTermica;
    private javax.swing.JLabel jlProcessador;
    private javax.swing.JLabel jlTemperaturaAmbiente;
    private javax.swing.JLabel jlUnidadeTemperaturaAmbiente;
    private javax.swing.JMenu jmAjuda;
    private javax.swing.JMenu jmBackup;
    private javax.swing.JMenu jmDissipador;
    private javax.swing.JMenu jmFicheiro;
    private javax.swing.JMenu jmIdioma;
    private javax.swing.JMenu jmPastaTermica;
    private javax.swing.JMenu jmProcessador;
    private javax.swing.JMenuItem jmiAdicionarDissipador;
    private javax.swing.JMenuItem jmiAdicionarPTermica;
    private javax.swing.JMenuItem jmiAdicionarProcessador;
    private javax.swing.JMenuItem jmiAjudaManual;
    private javax.swing.JMenuItem jmiAjudaSobre;
    private javax.swing.JMenuItem jmiBackupCarregar;
    private javax.swing.JMenuItem jmiBackupSair;
    private javax.swing.JMenuItem jmiEditarDissipador;
    private javax.swing.JMenuItem jmiEditarPTermica;
    private javax.swing.JMenuItem jmiEditarProcessador;
    private javax.swing.JMenuItem jmiRemoverDissipador;
    private javax.swing.JMenuItem jmiRemoverPTermica;
    private javax.swing.JMenuItem jmiRemoverProcessador;
    private javax.swing.JMenuItem jmiSair;
    private javax.swing.JSpinner jpTemperaturaAmbiente;
    private javax.swing.JLabel lblResposta;
    // End of variables declaration//GEN-END:variables
}
