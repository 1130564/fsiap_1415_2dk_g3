package fsiap2dkg3.gui;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.EditarDissipadorController;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.model.Dissipador;
import utils.Utils;
import javax.swing.JOptionPane;

/**
 * Editar dissipador
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class DialogEditarDissipador extends javax.swing.JDialog {

    /**
     * Valor de Retorno da Dialog
     */
    private Object information = false;

    /**
     * Controller
     */
    private EditarDissipadorController controller;

    /**
     * Construir Dialog
     *
     * @param parent Pai Dialog
     * @param modal Modal Mode
     * @param controller Controller
     */
    public DialogEditarDissipador(java.awt.Frame parent, boolean modal, EditarDissipadorController controller) {
        super(parent, modal);
        this.controller = controller;
        initComponents();
        updateValues();
        setVisible(true);
    }

    /**
     * Actualizar Valores dos Componentes
     */
    public final void updateValues() {
        Dissipador dissipador = this.controller.getDissipador();
        txtNome.setText(dissipador.getNome());
        txtTipo.setText(dissipador.getTipo());
        txtCondutividadeTermica.setValue(dissipador.getCondutividadeTermica());
        txtEspessura.setValue(dissipador.getEspessura());
    }

    /**
     * Valor de Retorno da Daiog
     *
     * @return Sucesso da Operação(FALSE=Operação não foi Concluída com Sucesso)
     */
    public Object getInformation() {
        return this.information;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlNome = new javax.swing.JLabel();
        lblTipo = new javax.swing.JLabel();
        lblCondutividade = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        txtTipo = new javax.swing.JFormattedTextField();
        txtCondutividadeTermica = new javax.swing.JFormattedTextField();
        jlUnidadeCondutividade = new javax.swing.JLabel();
        jbAdicionar = new javax.swing.JButton();
        jbCancelar = new javax.swing.JButton();
        jlEspessura = new javax.swing.JLabel();
        txtEspessura = new javax.swing.JFormattedTextField();
        jlUnidadeEspessura = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("fsiap2dkg3/dicionario/EditarDissipador"); // NOI18N
        setTitle(bundle.getString("DialogEditarDissipador.title")); // NOI18N
        setResizable(false);

        jlNome.setText(bundle.getString("DialogEditarDissipador.jlNome.text")); // NOI18N

        lblTipo.setText(bundle.getString("DialogEditarDissipador.lblTipo.text")); // NOI18N

        lblCondutividade.setText(bundle.getString("DialogEditarDissipador.lblCondutividade.text")); // NOI18N

        txtNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNomeActionPerformed(evt);
            }
        });

        txtTipo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        txtCondutividadeTermica.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        jlUnidadeCondutividade.setText(bundle.getString("DialogEditarDissipador.jlUnidadeCondutividade.text")); // NOI18N

        jbAdicionar.setText(bundle.getString("DialogEditarDissipador.jbAdicionar.text")); // NOI18N
        jbAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbAdicionarActionPerformed(evt);
            }
        });

        jbCancelar.setText(bundle.getString("DialogEditarDissipador.jbCancelar.text")); // NOI18N
        jbCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCancelarActionPerformed(evt);
            }
        });

        jlEspessura.setText(bundle.getString("DialogEditarDissipador.jlEspessura.text")); // NOI18N

        txtEspessura.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        jlUnidadeEspessura.setText(bundle.getString("DialogEditarDissipador.jlUnidadeEspessura.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblTipo)
                            .addComponent(jlNome)
                            .addComponent(lblCondutividade)
                            .addComponent(jlEspessura))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtTipo)
                            .addComponent(txtCondutividadeTermica)
                            .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEspessura, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(5, 5, 5)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlUnidadeCondutividade)
                            .addComponent(jlUnidadeEspessura)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addComponent(jbAdicionar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbCancelar)))
                .addContainerGap(42, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlNome)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTipo)
                    .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCondutividade)
                    .addComponent(txtCondutividadeTermica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlUnidadeCondutividade))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlEspessura, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtEspessura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jlUnidadeEspessura)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbCancelar)
                    .addComponent(jbAdicionar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jbCancelar.getAccessibleContext().setAccessibleName(bundle.getString("DialogEditarDissipador.jbCancelar.AccessibleContext.accessibleName")); // NOI18N

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Cancelar Operação
     *
     * @param evt
     */
    private void jbCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCancelarActionPerformed
        this.information = false;
        dispose();
    }//GEN-LAST:event_jbCancelarActionPerformed

    /**
     * Concluir Operação
     *
     * @param evt
     */
    private void jbAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbAdicionarActionPerformed
        try {
            if (txtNome.getText().equals("")
                    || txtTipo.getText().equals("")
                    || txtCondutividadeTermica.getValue() == null
                    || txtEspessura.getValue() == null) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado("Erro")+": "+Dicionario.getSignificado("DadosInvalidos")+"!", getTitle(), JOptionPane.WARNING_MESSAGE);
                return;
            }
            if (!Utils.confirmar()) {
                return;
            }

            String nome = txtNome.getText();
            String tipo = txtTipo.getText();
            float condutividade = Float.parseFloat(txtCondutividadeTermica.getValue().toString());
            float espessura = Float.parseFloat(txtEspessura.getValue().toString());

            if (this.controller.editDissipador(nome, tipo, condutividade, espessura)) {
                this.information = true;
                dispose();
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado("Erro")+": "+Dicionario.getSignificado("DadosInvalidos")+"!", getTitle(), JOptionPane.WARNING_MESSAGE);
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado("Erro")+": " + ex + "!", getTitle(), JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jbAdicionarActionPerformed

    private void txtNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNomeActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jbAdicionar;
    private javax.swing.JButton jbCancelar;
    private javax.swing.JLabel jlEspessura;
    private javax.swing.JLabel jlNome;
    private javax.swing.JLabel jlUnidadeCondutividade;
    private javax.swing.JLabel jlUnidadeEspessura;
    private javax.swing.JLabel lblCondutividade;
    private javax.swing.JLabel lblTipo;
    private javax.swing.JFormattedTextField txtCondutividadeTermica;
    private javax.swing.JFormattedTextField txtEspessura;
    private javax.swing.JTextField txtNome;
    private javax.swing.JFormattedTextField txtTipo;
    // End of variables declaration//GEN-END:variables
}
