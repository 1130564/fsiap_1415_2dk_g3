package fsiap2dkg3.exportar.sistema;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import utils.Utils;
import fsiap2dkg3.model.Sistema;
import fsiap2dkg3.exception.ExportarException;
import fsiap2dkg3.exception.ExtensaoException;

/**
 * Exportar Sistema
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class ExportarSistema {

    public final static List<ExportarSistemaType> TYPES = new ArrayList<>(Arrays.asList(
            new ExportarSistemaBAK(),
            new ExportarSistemaTXT()
    ));

    public static boolean exportar(File file, Sistema sistema) throws ExtensaoException, ExportarException {
        return exportar(file, sistema, true);
    }

    public static boolean exportar(File file, Sistema sistema, boolean log) throws ExtensaoException, ExportarException {
        String ext = Utils.getExtensao(file);
        if (ext != null) {
            for (ExportarSistemaType type : TYPES) {
                if (type.getType().equalsIgnoreCase(ext)) {
                    return type.exportar(file, sistema, log);
                }
            }
        }
        throw new ExtensaoException("Extensão Inválida");
    }
}
