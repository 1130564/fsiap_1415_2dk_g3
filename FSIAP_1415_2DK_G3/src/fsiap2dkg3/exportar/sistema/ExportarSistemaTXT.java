package fsiap2dkg3.exportar.sistema;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;

import fsiap2dkg3.VARS;
import fsiap2dkg3.exception.ExportarException;
import fsiap2dkg3.exportar.relatorio.ExportarRelatorio;
import fsiap2dkg3.model.Sistema;
import java.util.Formatter;

/**
 * Exportar Sistema em Formato Texto
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class ExportarSistemaTXT implements ExportarSistemaType {

    private final String type = "TXT";

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public boolean exportar(File file, Sistema sistema) throws ExportarException {
        return this.exportar(file, sistema, VARS.LOG);
    }

    @Override
    public boolean exportar(File file, Sistema sistema, boolean log) throws ExportarException {
        String erro = "";
        try (Formatter out = new Formatter(file)) {
            out.format("%s\n", sistema.toString());
            out.close();
        } catch (Exception ex) {
            erro = ex.getMessage();
        }
        ExportarRelatorio.exportar(VARS.LOGFILE, "[EXPORTAR][SISTEMA];[" + file.getAbsolutePath() + "];[TXT];[" + new Date() + "]" + "[" + (erro.equals("") ? "OK" : "ERRO(" + erro + ")") + "]", log);
        if (erro.equals("")) {
            return true;
        }
        throw new ExportarException(erro);
    }

}
