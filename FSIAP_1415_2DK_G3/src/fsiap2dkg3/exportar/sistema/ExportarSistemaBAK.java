package fsiap2dkg3.exportar.sistema;

import fsiap2dkg3.VARS;
import java.io.File;

import fsiap2dkg3.exception.ExportarException;
import fsiap2dkg3.exportar.relatorio.ExportarRelatorio;
import fsiap2dkg3.model.Sistema;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

/**
 * Exportar Sistema em Formato Binario
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class ExportarSistemaBAK implements ExportarSistemaType {

    private final String type = "BAK";

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public boolean exportar(File file, Sistema empresa) throws ExportarException {
        return this.exportar(file, empresa, VARS.LOG);
    }

    @Override
    public boolean exportar(File file, Sistema empresa, boolean log) throws ExportarException {
        String erro = "";
        try {
            FileOutputStream fileOut = new FileOutputStream(file.getAbsoluteFile());
            ObjectOutputStream outStream = new ObjectOutputStream(fileOut);
            outStream.writeObject(empresa);
            outStream.close();
            fileOut.close();
        } catch (Exception ex) {
            erro = ex.getMessage();
        }
        ExportarRelatorio.exportar(VARS.LOGFILE, "[EXPORTAR][SISTEMA];[" + file.getAbsolutePath() + "];[BAK];[" + new Date() + "]" + "[" + (erro.equals("") ? "OK" : "ERRO(" + erro + ")") + "]", log);
        if (erro.equals("")) {
            return true;
        }
        throw new ExportarException(erro);
    }

}
