package fsiap2dkg3.exportar.relatorio;

import java.io.File;

import fsiap2dkg3.exception.ExportarException;

/**
 * Exportar Relatorio (ESTRUTURA)
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public interface ExportarRelatorioType {
    public String getType();
    public boolean exportar(File file, String strLog) throws ExportarException;
    public boolean exportar(File file,String strLog, boolean log) throws ExportarException;
}
