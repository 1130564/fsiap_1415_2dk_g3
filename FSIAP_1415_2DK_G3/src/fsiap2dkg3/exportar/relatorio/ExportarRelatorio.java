package fsiap2dkg3.exportar.relatorio;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import utils.Utils;
import fsiap2dkg3.exception.ExportarException;

/**
 * Exportar Relatorio
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class ExportarRelatorio {

    public final static List<ExportarRelatorioType> TYPES = new ArrayList<ExportarRelatorioType>(Arrays.asList(
            new ExportarRelatorioLOG()
    ));

    public static boolean exportar(File file, String strLog) {
        return exportar(file, strLog, true);
    }

    public static boolean exportar(File file, String strLog, boolean log) {
        String ext = Utils.getExtensao(file);
        if (ext != null) {
            try {
                for (ExportarRelatorioType type : TYPES) {
                    if (type.getType().equalsIgnoreCase(ext)) {
                        return type.exportar(file, strLog, log);
                    }
                }
            } catch (ExportarException ex) {
            }
        }
        return false;
    }
}
