package fsiap2dkg3.exportar.relatorio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import fsiap2dkg3.VARS;
import fsiap2dkg3.exception.ExportarException;

/**
 * Exportar Relatorio em Formato LOG (ESTRUTURA)
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class ExportarRelatorioLOG implements ExportarRelatorioType {

    private final String type = "LOG";

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public boolean exportar(File file, String strLog) throws ExportarException {
        return exportar(file, strLog, VARS.LOG);
    }

    @Override
    public boolean exportar(File file, String strLog, boolean log) throws ExportarException {
        file = (file == null) ? VARS.LOGFILE : file;
        if (log == false) {
            return true;
        }
        String erro = "";
        try {
            FileWriter writer = new FileWriter(file, true);
            BufferedWriter outLog = new BufferedWriter(writer);
            writer.append("\n" + strLog);
            outLog.close();
        } catch (Exception ex) {
            erro = ex.getMessage();
        }
        throw new ExportarException(erro);
    }

}
