/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.VARS;
import fsiap2dkg3.model.PastaTermica;
import java.util.List;


 /**
 * UC9 - Remover Pasta Térmica Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class RemoverPastaTermicaController {
    /**
     * Pasta térmica a Remover
     */
    private PastaTermica pastaTermica;
    
    /**
     * Lista de pastas térmicas do Sistema
     * @return Lista de pastas térmicas
     */
    public List<PastaTermica> getPastasTermicas(){
        return VARS.SISTEMA.getRegistoPastasTermicas().getPastasTermicas();
    }
    
    /**
     * Selecionar Pasta térmica 
     * @param pt pasta térmica
     */
    public void setPastaTermica(PastaTermica pt){
        this.pastaTermica=pt;
    }
    
    /**
     * Remover PastaTermica
     * @return Sucesso da Operação
     */
    public boolean removerPastaTermica(){
        return VARS.SISTEMA.getRegistoPastasTermicas().rmPastaTermica(pastaTermica);
    }
    
}
