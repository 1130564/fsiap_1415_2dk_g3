package fsiap2dkg3.controller;

import fsiap2dkg3.VARS;
import fsiap2dkg3.hclc.RegistoProcessadores;
import fsiap2dkg3.model.Processador;

/**
 * UC1 - Adicionar Processador Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class AdicionarProcessadorController {
    
    /**
     * Processador a Adicionar
     */
    private Processador processador;
    
    /**
     * Criar Novo Processador
     * @param nome Nome do Processador
     * @param potenciaDissipada (TDP)Potencia Dissipada do Processador(W)
     * @param maxTemperatura Termperatura Maxima do Processador(ºC)
     * @param area Area do Processador(m^2)
     * @return Novo Processador
     */
    public Processador novoProcessador(String nome, float potenciaDissipada,float maxTemperatura,float area){
        return this.processador= RegistoProcessadores.novoProcessador(nome,potenciaDissipada,maxTemperatura,area);
    }
    
    /**
     * Adicionar Processador no Sistema
     * @return Sucesso da Operação 
     */
    public boolean registaProcessador(){
        return VARS.SISTEMA.getRegistoProcessadores().addProcessador(this.processador);
    }
}
