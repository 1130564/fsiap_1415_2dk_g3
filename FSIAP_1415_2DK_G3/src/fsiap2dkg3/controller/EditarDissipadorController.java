package fsiap2dkg3.controller;

import java.util.List;

import fsiap2dkg3.VARS;
import fsiap2dkg3.model.Dissipador;
import fsiap2dkg3.model.Processador;

/**
 * UC2 - Editar Dissipador Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class EditarDissipadorController {
    /**
     * Dissipador a Remover
     */
    private Dissipador dissipador;
    
    /**
     * Lista de Processadors do Sistema
     * @return Lista de Processadores
     */
    public List<Dissipador> getDissipadores(){
        return VARS.SISTEMA.getRegistoDissipadores().getDissipadores();
    }
    
    /**
     * Selecionar Dissipador
     * @param dissipador Dissipador
     */
    public void setDissipador(Dissipador dissipador){
        this.dissipador=dissipador;
    }
    
    /**
     * Consultar Dissipador Selecionado
     * @return Dissipador
     */
    public Dissipador getDissipador(){
        return this.dissipador;
    }
    
    /**
     * Editar dissipador
     * @param nome nome do dissipador
     * @param tipo tipo do dissipador
     * @param condutividade condutividade do dissipador
     * @param espessura espessura do dissipador
     * @return sucesso da operaçao
     */
    public boolean editDissipador(String nome, String tipo, float condutividade, float espessura){
        return VARS.SISTEMA.getRegistoDissipadores().editDissipador(this.dissipador, nome, tipo, condutividade, espessura);
    }
}
