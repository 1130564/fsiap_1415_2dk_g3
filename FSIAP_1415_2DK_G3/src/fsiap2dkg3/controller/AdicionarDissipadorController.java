/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.VARS;
import fsiap2dkg3.hclc.RegistoDissipadores;
import fsiap2dkg3.model.Dissipador;

/**
 * UC - Adicionar Dissipador Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class AdicionarDissipadorController {
    
    /**
     * Processador a Adicionar
     */
    private Dissipador dissipador;
    /**
     * Cria e retorna um novo dissipador
     * @param nome nome a dar ao dissipador
     * @param tipo tipo de dissipador (cobre, ouro, prata...)
     * @param condutividadeTermica condutividade do dissipador  
     * @param espessura espessura do dissipador
     * @return dissipador criado
     */
    
    public Dissipador novoDissipador(String nome, String tipo, float condutividadeTermica, float espessura){
        return this.dissipador= RegistoDissipadores.novoDissipador(nome, tipo, condutividadeTermica, espessura);
    }
    
    /**
     * Adicionar Dissipador no Sistema
     * @return Sucesso da Operação 
     */
    public boolean registaDissipador(){
        return VARS.SISTEMA.getRegistoDissipadores().addDissipador(this.dissipador);
    }
}
