package fsiap2dkg3.controller;

import java.util.List;

import fsiap2dkg3.VARS;
import fsiap2dkg3.model.Processador;

/**
 * UC3 - Remover Processador Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class RemoverProcessadorController {
    /**
     * Processador a Remover
     */
    private Processador processador;
    
    /**
     * Lista de Processadors do Sistema
     * @return Lista de Processadores
     */
    public List<Processador> getProcessadores(){
        return VARS.SISTEMA.getRegistoProcessadores().getProcessadores();
    }
    
    /**
     * Selecionar Processador
     * @param processador Processador
     */
    public void setProcessador(Processador processador){
        this.processador=processador;
    }
    
    /**
     * Remocer Processador
     * @return Sucesso da Operação
     */
    public boolean removerProcessador(){
        return VARS.SISTEMA.getRegistoProcessadores().rmProcessador(this.processador);
    }
    
}
