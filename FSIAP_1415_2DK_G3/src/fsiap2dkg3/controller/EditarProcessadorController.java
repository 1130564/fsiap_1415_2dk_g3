package fsiap2dkg3.controller;

import java.util.List;

import fsiap2dkg3.VARS;
import fsiap2dkg3.model.Processador;

/**
 * UC2 - Editar Processador Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class EditarProcessadorController {
    /**
     * Processador a editar
     */
    private Processador processador;
    
    /**
     * Lista de Processadors do Sistema
     * @return Lista de Processadores
     */
    public List<Processador> getProcessadores(){
        return VARS.SISTEMA.getRegistoProcessadores().getProcessadores();
    }
    
    /**
     * Selecionar Processador
     * @param processador Processador
     */
    public void setProcessador(Processador processador){
        this.processador=processador;
    }
    
    /**
     * Consultar Processador Selecionado
     * @return Processador
     */
    public Processador getProcessador(){
        return this.processador;
    }
    
    /**
     * Editar Processador 
    * @param nome Novo Nome do Processador
     * @param potenciaDissipada Nova (TDP)Potencia Dissipada do Processador(W)
     * @param maxTemperatura Nova Termperatura Maxima do Processador(ºC)
     * @param area Nova Area do Processador(m^2)
     * @return Sucesso da Operação
     */
    public boolean editProcessador(String nome, float potenciaDissipada, float maxTemperatura, float area){
        return VARS.SISTEMA.getRegistoProcessadores().editProcessador(this.processador, nome, potenciaDissipada, maxTemperatura, area);
    }
}
