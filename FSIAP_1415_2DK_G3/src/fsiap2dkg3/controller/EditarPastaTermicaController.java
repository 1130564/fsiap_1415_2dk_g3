/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.VARS;
import fsiap2dkg3.model.PastaTermica;
import java.util.List;

/**
 * UC8 - Editar Pasta Termica Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class EditarPastaTermicaController {
    /**
     * Processador a editar
     */
    private PastaTermica pastaTermica;
    
    /**
     * Lista de Pastas Térmicas do Sistema
     * @return Lista de Processadores
     */
    public List<PastaTermica> getPastasTermicas(){
        return VARS.SISTEMA.getRegistoPastasTermicas().getPastasTermicas();
    }
    
    /**
     * Selecionar Pasta Térmica
     * @param pt pasta térmica  
     */
    public void setPastaTermica(PastaTermica pt){
        this.pastaTermica=pt;
    }
    
    /**
     * Consultar Pasta Térmica Selecionado
     * @return Pasta Térmica
     */
    public PastaTermica getPastaTermica(){
        return this.pastaTermica;
    }
    
    /**
     * Editar Pasta Térmica
     * @param nome nome da pasta térmica.
     * @param condutividadeTermica conduvidade do pasta térmica.
     * @param espessura espessura da pasta térmica
     * @return sucesso da operação
     */
    public boolean editPastaTermica(String nome,float condutividadeTermica,float espessura){
        return VARS.SISTEMA.getRegistoPastasTermicas().editPastaTermica(pastaTermica, nome, condutividadeTermica, espessura);
    }
}
