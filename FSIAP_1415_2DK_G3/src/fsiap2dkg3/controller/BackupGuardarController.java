package fsiap2dkg3.controller;

import fsiap2dkg3.VARS;
import fsiap2dkg3.exportar.sistema.ExportarSistema;
import java.io.File;

/**
 * UC12 - Persistência dos Dados(Guardar) Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class BackupGuardarController {
     public boolean guardarBackup(File file) {
        try {
            ExportarSistema.exportar(file, VARS.SISTEMA);
            VARS.SISTEMAFILE = file;
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
