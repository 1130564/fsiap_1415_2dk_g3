/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.VARS;
import fsiap2dkg3.importar.sistema.ImportarSistema;
import fsiap2dkg3.model.Sistema;
import java.io.File;

/**
 * UC13 - Persistência dos Dados(Carregar) Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class BackupCarregarController {
     /**
     * Carrega o Backup
     *
     * @param file Ficheiro do Backup
     * @return true caso seja Carregado. Caso contrario, retorna false
     */
    public boolean carregarBackup(File file) {
        try {
            Sistema empresa = ImportarSistema.importar(file).getSistema();
            VARS.SISTEMAFILE = file;
            VARS.SISTEMA = empresa;
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
