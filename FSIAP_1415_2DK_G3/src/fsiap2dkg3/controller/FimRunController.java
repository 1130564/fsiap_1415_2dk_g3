package fsiap2dkg3.controller;

import fsiap2dkg3.VARS;

/**
 * (Fim da Aplicação)Fim Run Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class FimRunController {
    
    /**
     * Guardar
     * @return o sucesso da operaçao  
     */
  public boolean save(){
      boolean returnSave = VARS.save(null);
      return returnSave;
  }
}