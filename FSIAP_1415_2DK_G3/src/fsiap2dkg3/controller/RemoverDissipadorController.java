package fsiap2dkg3.controller;

import java.util.List;

import fsiap2dkg3.VARS;
import fsiap2dkg3.model.Dissipador;

/**
 * Remover Dissipador Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class RemoverDissipadorController {
    /**
     * Processador a Remover
     */
    private Dissipador dissipador;
    
    /**
     * Lista de Dissipadores do Sistema
     * @return Lista de Dissipadores
     */
    public List<Dissipador> getDissipadores(){
        return VARS.SISTEMA.getRegistoDissipadores().getDissipadores();
    }
    
    /**
     * Selecionar Dissipador
     * @param dissipador dissipador
     */
    public void setDissipador(Dissipador dissipador){
        this.dissipador=dissipador;
    }
    
    /**
     * Remover Dissipador
     * @return Sucesso da Operação
     */
    public boolean removerDissipador(){
        return VARS.SISTEMA.getRegistoDissipadores().rmDissipador(this.dissipador);
    }
    
}
