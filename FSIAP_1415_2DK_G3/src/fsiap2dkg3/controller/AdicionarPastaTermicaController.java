/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.controller;

import fsiap2dkg3.VARS;
import fsiap2dkg3.hclc.RegistoPastasTermicas;
import fsiap2dkg3.model.PastaTermica;

/**
 * UC7 - Adicionar Pasta Termica Controller
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class AdicionarPastaTermicaController {
    
    /**
     * Pasta térmica a adicionar
     */
    private PastaTermica pastaTermica;
    
    /**
     * Criar Nova Pasta Térmica
     * @param nome Nome da Pasta Térmica
     * @param condutividadeTermica condutividade da Pasta Térmica
     * @param espessura espesssura da Pasta Térmica
     * @return Nova Pasta Térmica
     */
    public PastaTermica novoPastaTermica(String nome,float condutividadeTermica,float espessura){
        return this.pastaTermica= RegistoPastasTermicas.novoPastaTermica(nome, condutividadeTermica, espessura);
    }
    
    /**
     * Adicionar Pasta Térmica no Sistema
     * @return Sucesso da Operação 
     */
    public boolean registaPastaTermica(){
        return VARS.SISTEMA.getRegistoPastasTermicas().addPastaTermica(this.pastaTermica);
    }
    
}
