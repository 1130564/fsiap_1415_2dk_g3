package fsiap2dkg3;

import java.awt.Frame;
import java.io.File;
import java.util.Scanner;
import java.util.Formatter;

import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.model.Sistema;
import utils.Utils;
import fsiap2dkg3.exportar.sistema.ExportarSistema;
import fsiap2dkg3.importar.sistema.ImportarSistema;

/**
 * Variáveis de Sistema
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class VARS {

    /**
     * Ficheiro de Configurações Pré-Definido
     */
    public static File CONFIGFILE = new File("config.var");
    /**
     * Load do Sistema
     */
    public static boolean AUTOLOAD = false;
    /**
     * Load do Sistema
     */
    public static boolean AUTOSAVE = false;
    /**
     * Sistema do Sistema
     */
    public static Sistema SISTEMA = new Sistema();
    /**
     * Ficheiro Pré-Definido do Sistema
     */
    public static File SISTEMAFILE = new File("sistema.bak");
    ;
    /**
     * Gerar Ficheiro Logs Pré-Definido
     */
    public static boolean LOG = true;
    /**
     * Ficheiro de Logs
     */
    public static File LOGFILE = new File("resume.log");
    ;
    /**
     * Alterações das Variáveis de Sistema
     */
    public static boolean ALTERACOES = false;
    /**
     * Actual Frame de Sistema
     */
    public static Frame CURRENTFRAME = null;
    /**
     * Ultimo Directório Acedido
     */
    public static File LASTFILE = new File("");

    /**
     * Debug da Aplicação Pré-Definido
     */
    public static boolean DEBUG = false;

    /**
     * Carregar o Sistema
     *
     * @param fileConfig Ficheiro de Configuração
     * @return true caso seja realizado com sucesso o carregamento. Caso
     * Contrario, retorna false
     */
    public static boolean load(File fileConfig) {
        VARS.CONFIGFILE = (fileConfig == null) ? VARS.CONFIGFILE : fileConfig;
        try (Scanner in = new Scanner(VARS.CONFIGFILE)) {
            while (in.hasNext()) {
                try{
                String conf = in.nextLine().trim();
                    
                String[] splitConf = conf.split("=");
                
                if (splitConf.length >= 2) {
                    switch (splitConf[0].toLowerCase()) {
                        case "boolean(autoload)":
                            VARS.AUTOLOAD = Boolean.parseBoolean(splitConf[1]);
                            break;
                        case "boolean(autosave)":
                            VARS.AUTOSAVE = Boolean.parseBoolean(splitConf[1]);
                            break;
                        case "file(backup)":
                            VARS.SISTEMAFILE = new File(Utils.pathRelativeToAbsolute(splitConf[1]));
                            break;
                        case "file(lastfile)":
                            VARS.LASTFILE = new File(Utils.pathRelativeToAbsolute(splitConf[1]));
                            break;
                        case "boolean(log)":
                            VARS.LOG = Boolean.parseBoolean(splitConf[1]);
                        case "file(logfile)":
                            VARS.LOGFILE = new File(Utils.pathRelativeToAbsolute(splitConf[1]));
                        case "string(lang)":
                            Dicionario.setLANG(splitConf[1]);
                        case "boolean(debug)":
                            VARS.DEBUG = Boolean.parseBoolean(splitConf[1]);
                    }
                }
                }catch (Exception ex) { 
                }
            }
            in.close();
        } catch (Exception ex) {
        }
        if (VARS.AUTOLOAD && VARS.SISTEMAFILE.isFile() && VARS.SISTEMAFILE.exists()) {
                try {
                    VARS.SISTEMA = ImportarSistema.importar(VARS.SISTEMAFILE).getSistema();
                } catch (Exception ex) {
                    return false;
                }
            }
            
        return true;
    }
    
     /**
     * Guardar o Sistema
     *
     * @param fileConfig Ficheiro de Configuração
     * @return true caso seja realizado com sucesso o carregamento. Caso
     * Contrario, retorna false
     */
    public static boolean save(File fileConfig) {
        VARS.CONFIGFILE = (fileConfig == null) ? CONFIGFILE : fileConfig;
        try {
            ExportarSistema.exportar(VARS.SISTEMAFILE, VARS.SISTEMA);
            try (Formatter out = new Formatter(VARS.CONFIGFILE)) {
                out.format("BOOLEAN(AutoLoad)=%s\n", VARS.AUTOLOAD);
                out.format("BOOLEAN(AutoSave)=%s\n", VARS.AUTOSAVE);
                out.format("FILE(Backup)=%s\n", Utils.pathAbsoluteToRelative(VARS.SISTEMAFILE.getAbsoluteFile().toString()));
                out.format("FILE(LastFile)=%s\n", Utils.pathAbsoluteToRelative(VARS.LASTFILE.getAbsoluteFile().toString()));
                out.format("BOOLEAN(Log)=%s\n", VARS.LOG);
                out.format("FILE(LogFile)=%s\n", Utils.pathAbsoluteToRelative(VARS.LOGFILE.getAbsoluteFile().toString()));
                out.format("STRING(lang)=%s\n", Dicionario.getLANG());
                out.format("BOOLEAN(Debug)=%s\n", VARS.DEBUG);
                out.close();
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }
    
}
