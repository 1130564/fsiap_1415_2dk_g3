/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.model;

/**
 *
 * @author gilbertopereira
 */
public class Calculo {
    private static float calcResistencia(float espessura, float conduvitividade, float area) {
        return espessura / (conduvitividade * area);
    }

    public static float calcTemp(Processador p, PastaTermica pt, Dissipador d, float tempMaquina) {
        float rpt = calcResistencia(pt.getEspessura(), pt.getCondutividadeTermica(), p.getArea());
        float rd = calcResistencia(d.getEspessura(), d.getCondutividadeTermica(), p.getArea());

        return p.getPotenciaDissipada()* (rpt + rd) + tempMaquina;
    }
}
