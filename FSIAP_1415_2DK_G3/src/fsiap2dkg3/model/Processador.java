package fsiap2dkg3.model;

import java.io.Serializable;

/**
 * Representação de um Processador
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class Processador implements Serializable {

    // Variáveis de Instância \\
    /**
     * Nome do Processador
     */
    private String nome;

    /**
     * (TDP)Potencia Dissipada do Processador(W)
     */
    private float potenciaDissipada;

    /**
     * Termperatura Maxima do Processador(ºC)
     */
    private float maxTemperatura;

    /**
     * Area do Processador(m^2)
     */
    private float area;

    // Constantes de Classe \\
    // Variáveis de Classe \\
    /**
     * Nome Pré-Definido
     */
    public static String NOME = "não definido";

    /**
     * (TDP)Potencia Dissipada Pré-Definida(W)
     */
    public static float POTENCIADISSIPADA = 0;

    /**
     * Termperatura Maxima Pré-Definida(ºC)
     */
    public static float MAXTEMPERATURA = 0;

    /**
     * Area Pré-Definida(m^2)
     */
    public static float AREA = 0;
    
    // Construtores \\
    /**
     * Construir Instância do Processador (COMPLETO)
     *
     * @param nome Nome do Processador
     * @param potenciaDissipada (TDP)Potencia Dissipada do Processador(W)
     * @param maxTemperatura Termperatura Maxima do Processador(ºC)
     * @param area Area do Processador(m^2)
     */
    public Processador(String nome, float potenciaDissipada, float maxTemperatura, float area) {
        this.nome = nome;
        this.potenciaDissipada = potenciaDissipada;
        this.maxTemperatura = maxTemperatura;
        this.area = area;
    }

    /**
     * Construir Instância do Processador (CÓPIA)
     *
     * @param copia Processador com as Características a Copiar
     */
    public Processador(Processador copia) {
        this(copia.nome, copia.potenciaDissipada, copia.maxTemperatura, copia.area);
    }

    /**
     * Construir Instância do Processador (PRÉ-DEFINIDO)
     */
    public Processador() {
        this(Processador.NOME, Processador.POTENCIADISSIPADA, Processador.MAXTEMPERATURA, Processador.AREA);
    }

    // Métodos de Instância \\
    /**
     * Consultar Nome do Processador
     *
     * @return Nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Consultar (TDP)Potencia Dissipada do Processador(W)
     *
     * @return (TDP)Potencia Dissipada(W)
     */
    public float getPotenciaDissipada() {
        return potenciaDissipada;
    }

    /**
     * Consultar Termperatura Maxima do Processador(ºC)
     *
     * @return Termperatura Maxima(ºC)
     */
    public float getMaxTemperatura() {
        return maxTemperatura;
    }

    /**
     * Consultar Area do Processador(m^2)
     *
     * @return Area(m^2)
     */
    public float getArea() {
        return area;
    }

    /**
     * Alterar Nome do Processador
     *
     * @param nome Novo Nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Alterar (TDP)Potencia Dissipada do Processador(W)
     *
     * @param potenciaDissipada Nova (TDP)Potencia Dissipada(W)
     */
    public void setPotenciaDissipada(float potenciaDissipada) {
        this.potenciaDissipada = potenciaDissipada;
    }

    /**
     * Alterar Termperatura Maxima do Processador(ºC)
     *
     * @param maxTemperatura Nova Termperatura Maxima(ºC)
     */
    public void setMaxTemperatura(float maxTemperatura) {
        this.maxTemperatura = maxTemperatura;
    }

    /**
     * Alterar Area do Processador(m^2)
     *
     * @param area Nova Area(m^2)
     */
    public void setArea(float area) {
        this.area = area;
    }
    
    /**
     * Representação Textual da Instância Formatada
     *
     * @return Representação Textual do Processador
     */
    @Override
    public String toString() {
        return "Nome: " + this.nome
                + "\n(TDP)Potencia Dissipada: " + this.potenciaDissipada+"w"
                + "\nTermperatura Maxima do Processador: " + this.maxTemperatura + "ºC"
                + "\nArea: " + this.area + "m^2";
    }
    
     /**
     * Compara o Processador com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Processador equivalente
     * ao Processador. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        Processador p = (Processador) outro;
        return this.nome.equals(p.nome)
                && this.potenciaDissipada==p.potenciaDissipada
                && this.maxTemperatura==p.maxTemperatura
                && this.area==p.area; 
    }
    
    /**
     * Validar o Processador
     *
     * @return true se for um Processador Válido. Caso contrario retorna false
     */
    public boolean valida() {
        return !this.nome.equals("")
                && this.potenciaDissipada!=0
                && this.maxTemperatura!=0
                && this.area!=0;
                
    }
    
    // Métodos de Classe \\
    // Métodos Privados \\

}
