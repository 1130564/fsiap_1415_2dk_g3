package fsiap2dkg3.model;

import java.io.Serializable;

/**
 * Representação de um Pasta Térmica
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class PastaTermica implements Serializable {

    // Variáveis de Instância \\
    /**
     * Nome da Pasta Térmica
     */
    private String nome;

    /**
     * Condutividade Termica da Pasta Térmica(W/m.c)
     */
    private float condutividadeTermica;

    /**
     * Espessura da Pasta Térmica(m)
     */
    private float espessura;

    // Constantes de Classe \\
    // Variáveis de Classe \\
    /**
     * Nome Pré-Definido
     */
    public static String NOME = "não definido";

    /**
     * Condutividade Termica Pré-Definida(W/m.c)
     */
    public static float CONDUTIVIDADETERMICA = 0;

    /**
     * Espessura Pré-Definida(m)
     */
    public static float ESPESSURA=0;

    
    // Construtores \\
    /**
     * Construir Instância do Processador (COMPLETO)
     *
     * @param nome Nome da Pasta Térmica
     * @param condutividadeTermica Condutividade Termica da Pasta Térmica(W/m.c)
     * @param espessura Espessura da Pasta Térmica(m)
     */
    public PastaTermica(String nome, float condutividadeTermica, float espessura) {
        this.nome = nome;
        this.condutividadeTermica = condutividadeTermica;
        this.espessura = espessura;
    }

    /**
     * Construir Instância da Pasta Térmica (CÓPIA)
     *
     * @param copia Pasta Térmica com as Características a Copiar
     */
    public PastaTermica(PastaTermica copia) {
        this(copia.nome, copia.condutividadeTermica, copia.espessura);
    }
    
    public PastaTermica(){
        this(PastaTermica.NOME,PastaTermica.CONDUTIVIDADETERMICA,PastaTermica.ESPESSURA);
    }

    // Métodos de Instância \\
    /**
     * Consultar Nome da Pasta Térmica
     *
     * @return Nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Consultar Condutividade Termica da Pasta Térmica(W/m.c)
     *
     * @return Condutividade Termica(W/m.c)
     */
    public float getCondutividadeTermica() {
        return condutividadeTermica;
    }

    /**
     * Consultar Espessura da Pasta Térmica(m)
     *
     * @return Espessura(m)
     */
    public float getEspessura() {
        return espessura;
    }

    /**
     * Alterar Nome da Pasta Térmica
     *
     * @param nome Novo Nome
     */
    public final void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Alterar Condutividade Termica da Pasta Térmica(W/m.c)
     *
     * @param condutividadeTermica Nova Condutividade Termica(W/m.c)
     */
    public final void setCondutividadeTermica(float condutividadeTermica) {
        this.condutividadeTermica = condutividadeTermica;
    }

    /**
     * Alterar Espessura da Pasta Térmica(m)
     *
     * @param espessura Nova Espessura(m)
     */
    public final void setEspessura(float espessura) {
        this.espessura = espessura;
    }

    /**
     * Representação Textual da Instância Formatada
     *
     * @return Representação Textual da Pasta Térmica
     */
    @Override
    public String toString() {
        return "Nome: " + this.nome
                + "\nCondutividade Termica: " + this.condutividadeTermica + "W/m.c"
                + "\nEspessura: " + this.espessura + "mm";
    }

    /**
     * Compara o Pasta Térmica com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Pasta Térmica
     * equivalente ao Pasta Térmica. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        PastaTermica pt = (PastaTermica) outro;
        return this.nome.equals(pt.nome)
                && this.condutividadeTermica == pt.condutividadeTermica
                && this.espessura == pt.espessura;
    }

    /**
     * Validar a Pasta Térmica
     *
     * @return true se for uma Pasta Térmica Válido. Caso contrario retorna
     * false
     */
    public boolean valida() {
        return !this.nome.equals("")
                && this.condutividadeTermica != 0
                && this.espessura != 0;

    }

    // Métodos de Classe \\
    // Métodos Privados \\
}
