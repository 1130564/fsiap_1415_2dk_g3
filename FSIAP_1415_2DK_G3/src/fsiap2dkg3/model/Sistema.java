package fsiap2dkg3.model;

import fsiap2dkg3.hclc.*;
import java.io.Serializable;

/**
 * Representação de um Sistema
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class Sistema implements Serializable {

    // Variáveis de Instância \\
    /**
     * Registo de Processadores do Sistema
     */
    private RegistoProcessadores registoProcessadores;
    /**
     * Registo de Dissipadores do Sistema
     */
    private RegistoDissipadores registoDissipadores;
    /**
     * Registo de Pastas Termicas do Sistema
     */
    private RegistoPastasTermicas registoPastasTermicas;

    // Constantes de Classe \\
    // Variáveis de Classe \\
    /**
     * Registo de Processadores Pré-Definida
     */
    public static RegistoProcessadores REGISTOPROCESSADORES = new RegistoProcessadores();
    /**
     * Registo de Dissipadores Pré-Definida
     */
    public static RegistoDissipadores REGISTODISSIPADORES = new RegistoDissipadores();
    /**
     * Registo de Pastas Térmicas Pré-Definida
     */
    public static RegistoPastasTermicas REGISTOPASTASTERMICAS = new RegistoPastasTermicas();

    // Construtores \\
    /**
     * Construir Instância do Processador (COMPLETO)
     *
     * @param registoProcessadores Registo de Processadores do Sistema
     * @param registoDissipadores Registo de Dissipadores do Sistema
     * @param registoPastasTermicas Registo de Pastas Termicas do Sistema
     */
    public Sistema(RegistoProcessadores registoProcessadores, RegistoDissipadores registoDissipadores, RegistoPastasTermicas registoPastasTermicas) {
        this.setRegistoProcessadores(registoProcessadores);
        this.setRegistoDissipadores(registoDissipadores);
        this.setRegistoPastasTermicas(registoPastasTermicas);
    }

    /**
     * Construir Instância do Processador (CÓPIA)
     *
     * @param copia Sistema com as Características a Copiar
     */
    public Sistema(Sistema copia) {
        this(copia.registoProcessadores, copia.registoDissipadores, copia.registoPastasTermicas);
    }

    /**
     * Construir Instância do Processador (PRÉ-DEFINIDO)
     */
    public Sistema() {
        this(Sistema.REGISTOPROCESSADORES, Sistema.REGISTODISSIPADORES, Sistema.REGISTOPASTASTERMICAS);
    }

    /**
     * Consultar Registo de Processadores do Sistema
     *
     * @return Registo de Processadores
     */
    public RegistoProcessadores getRegistoProcessadores() {
        return registoProcessadores;
    }

    /**
     * Consultar Registo de Dissipadores do Sistema
     *
     * @return Registo de Dissipadores
     */
    public RegistoDissipadores getRegistoDissipadores() {
        return registoDissipadores;
    }

    /**
     * Consultar Registo de Pastas Térmicas do Sistema
     *
     * @return Registo de Pastas Térmicas
     */
    public RegistoPastasTermicas getRegistoPastasTermicas() {
        return registoPastasTermicas;
    }

    /**
     * Alterar Registo de Processadores do Sistema
     *
     * @param registoProcessadores Novo Registo de Processadores
     */
    public final void setRegistoProcessadores(RegistoProcessadores registoProcessadores) {
        if (registoProcessadores == null) {
            throw new IllegalArgumentException("Registo de Processadores Inválido");
        }
        this.registoProcessadores = new RegistoProcessadores(registoProcessadores);
    }

    /**
     * Alterar Registo de Dissipadores do Sistema
     *
     * @param registoDissipadores Novo Registo de Dissipadores
     */
    public final void setRegistoDissipadores(RegistoDissipadores registoDissipadores) {
        if (registoDissipadores == null) {
            throw new IllegalArgumentException("Registo de Disipadores Inválido");
        }
        this.registoDissipadores = new RegistoDissipadores(registoDissipadores);
    }

    /**
     * Alterar Registo de Pastas Termicas do Sistema
     *
     * @param registoPastasTermicas Novo Registo de Pastas Térmicas
     */
    public final void setRegistoPastasTermicas(RegistoPastasTermicas registoPastasTermicas) {
        if (registoDissipadores == null) {
            throw new IllegalArgumentException("Registo de Pastas Termicas Inválido");
        }
        this.registoPastasTermicas = new RegistoPastasTermicas(registoPastasTermicas);
    }

    /**
     * Representação Textual da Instância Formatada
     *
     * @return Representação Textual do Sistema
     */
    @Override
    public String toString() {
        return "Registo de Processadores:\n" + this.registoProcessadores.toString()
                + "\n\nRegisto de Disipadores:\n" + this.registoDissipadores.toString()
                + "\n\nRegisto de Pastas Térmicas:\n" + this.registoPastasTermicas.toString();
    }

    /**
     * Compara o Empresa com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Empresa equivalente
     * ao Empresa. Caso contrário, retorna false.
     */
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        Sistema o = (Sistema) outro;
        return this.registoProcessadores.equals(o.registoProcessadores)
                && this.registoDissipadores.equals(o.registoDissipadores)
                && this.registoPastasTermicas.equals(o.registoPastasTermicas);
    }

    /**
     * Carregar Data de Teste no Sistema
     */
    public void fillInData() {

    }

    // Métodos de Classe \\
}
