package fsiap2dkg3.model;

import java.io.Serializable;

/**
 * Representação de um Dissipador
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class Dissipador implements Serializable {

    // Variáveis de Instância \\
    /**
     * Nome do Dissipador
     */
    private String nome;
    /**
     * Tipo do Dissipador
     */
    private String tipo;
    /**
     * Condutividade Termica do Dissipador(W/m.c)
     */
    private float condutividadeTermica;
    /**
     * Espessura do Dissipador(m)
     */
    private float espessura;
   
    // Constantes de Classe \\
    // Variáveis de Classe \\
    /**
     * Nome Pré-Definido
     */
    public static String NOME = "não definido";
    /**
     * Tipo Pré-Definido
     */
    public static String TIPO = "não definido";
    /**
     * Condutividade Termica Pré-Definida(W/m.c)
     */
    public static float CONDUTIVIDADETERMICA = 0;
    /**
     * Espessura Pré-Definida (m)
     */
    public static float ESPESSURA = 0;

    // Construtores \\
    /**
     * Construir Instância do Dissipador (COMPLETO)
     *
     * @param nome Nome do Dissipador
     * @param tipo Tipo do Dissipador
     * @param condutividadeTermica Condutividade Termica do Dissipador(W/m.c)
     * @param espessura Espessura Termica do Dissipador(m)
     */
    public Dissipador(String nome, String tipo, float condutividadeTermica, float espessura) {
        this.nome = nome;
        this.tipo = tipo;
        this.condutividadeTermica = condutividadeTermica;
        this.espessura = espessura;
   
    }

    /**
     * Construir Instância do Dissipador (CÓPIA)
     *
     * @param copia Dissipador com as Características a Copiar
     */
    public Dissipador(Dissipador copia) {
        this(copia.nome, copia.tipo, copia.condutividadeTermica, copia.espessura);
    }

    /**
     * Construir Instância do Dissipador (PRÉ-DEFINIDO)
     */
    public Dissipador() {
        this(Dissipador.NOME, Dissipador.TIPO, Dissipador.CONDUTIVIDADETERMICA, Dissipador.ESPESSURA);
    }

    // Métodos de Instância \\
    /**
     * Consultar Nome do Dissipador
     *
     * @return Nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Consultar Tipo do Dissipador
     *
     * @return Tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Consultar Condutividade Termica do Dissipador(W/m.c)
     *
     * @return Condutividade Termica(W/m.c)
     */
    public float getCondutividadeTermica() {
        return condutividadeTermica;
    }

    /**
     * Consultar Espessura do Dissipador(m)
     *
     * @return Espessura(m)
     */
    public float getEspessura() {
        return espessura;
    }

    /**
     * Alterar Nome do Dissipador
     *
     * @param nome Novo Nome
     */
    public final void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Alterar Tipo do Dissipador
     *
     * @param tipo Novo Tipo
     */
    public final void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Alterar Condutividade Termica do Dissipador(W/m.c)
     *
     * @param condutividadeTermica Nova Condutividade Termica(W/m.c)
     */
    public final void setCondutividadeTermica(float condutividadeTermica) {
        this.condutividadeTermica = condutividadeTermica;
    }

    /**
     * Alterar Espessura do Dissipador(m)
     *
     * @param espessura Nova Espessura(m)
     */
    public final void setEspessura(float espessura) {
        this.espessura = espessura;
    }

 

    /**
     * Representação Textual da Instância Formatada
     *
     * @return Representação Textual do Dissipador
     */
    @Override
    public String toString() {
        return "Nome: " + this.nome
                + "\nTipo: " + this.tipo
                + "\nCondutividade Termica: " + this.condutividadeTermica + "W/m.c"
                + "\nEspessura: " + this.espessura + "m";
    }
    
     /**
     * Compara o Dissipador com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Dissipador equivalente
     * ao Dissipador. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        Dissipador d = (Dissipador) outro;
        return this.nome.equals(d.nome)
                && this.tipo.equals(d.tipo)
                && this.condutividadeTermica==d.condutividadeTermica
                && this.espessura==d.espessura;

    }
    
    /**
     * Validar o Dissipador
     *
     * @return true se for um Dissipador Válido. Caso contrario retorna false
     */
    public boolean valida() {
        return !this.nome.equals("")
                && !this.tipo.equals("")
                && this.condutividadeTermica!=0
                && this.espessura!=0;
                
    }
    
    // Métodos de Classe \\
    // Métodos Privados \\

}
