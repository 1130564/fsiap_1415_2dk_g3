package fsiap2dkg3.ui;


import fsiap2dkg3.VARS;
import javax.swing.JOptionPane;

import fsiap2dkg3.controller.AdicionarProcessadorController;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.gui.DialogAdicionarProcessador;

/**
 * UC1 - Adicionar Processador UI
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class AdicionarProcessadorUI {
    
    private AdicionarProcessadorController controller;
    
    public AdicionarProcessadorUI(){
        this.controller=new AdicionarProcessadorController();
    }
    
    /**
     * Correr Caso de Uso
     * @return Sucesso do Caso de Uso
     */
    public boolean run(){
        DialogAdicionarProcessador dialogAdicionarProcessador = new DialogAdicionarProcessador(null, true, this.controller);
        boolean returnDialogAdicionarProcessador = (boolean) dialogAdicionarProcessador.getInformation();
       
        if (returnDialogAdicionarProcessador) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"AdicionarProcessador", "Sucesso"), Dicionario.getSignificado(Dicionario.PATH+"AdicionarProcessador", "Titulo"), JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"AdicionarProcessador", "Insucesso"), Dicionario.getSignificado(Dicionario.PATH+"AdicionarProcessador", "Titulo"), JOptionPane.WARNING_MESSAGE);
        }
        
        return returnDialogAdicionarProcessador;
    }
}
