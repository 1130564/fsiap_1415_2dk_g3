/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.ui;


import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.EditarPastaTermicaController;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.gui.DialogEditarPastaTermica;
import fsiap2dkg3.model.PastaTermica;
import utils.Item;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Editar Dissipador UI
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class EditarPastaTermicaUI {
    
    
    private EditarPastaTermicaController controller;
    
    public EditarPastaTermicaUI(){
        this.controller=new EditarPastaTermicaController();
    }
    /**
     * Correr Caso de Uso
     *
     * @return Sucesso do Caso de Uso
     */
    public boolean run() {
        PastaTermica pt = selecionaPastaTermica();
        if (pt == null) {
            return false;
        }

        this.controller.setPastaTermica(pt);
        
        DialogEditarPastaTermica dialogEditarPastaTermica= new DialogEditarPastaTermica(VARS.CURRENTFRAME, true, this.controller);
        boolean returnDialogEditarPastaTermica= (boolean) dialogEditarPastaTermica.getInformation();
       
        if (returnDialogEditarPastaTermica) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarPastaTermica", "Sucesso"), Dicionario.getSignificado(Dicionario.PATH+"EditarPastaTermica", "Titulo"), JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarPastaTermica", "Insucesso"), Dicionario.getSignificado(Dicionario.PATH+"EditarPastaTermica", "Titulo"), JOptionPane.WARNING_MESSAGE);
        }
        
        return returnDialogEditarPastaTermica;
    }

    
    private PastaTermica selecionaPastaTermica() {
        
        List<PastaTermica> listaPastasTermicas = this.controller.getPastasTermicas();
        if (listaPastasTermicas.isEmpty()) {  //ALTERAR DICIONARIO!!
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarPastaTermica", "InvalidaListaPastasTermicas")+"!", Dicionario.getSignificado(Dicionario.PATH+"EditarPastaTermica", "Titulo"), JOptionPane.INFORMATION_MESSAGE);
            return null;
        }
        Object itemsEventos[] = new Object[listaPastasTermicas.size()];
        for (int i = 0; i < itemsEventos.length; i++) {
            itemsEventos[i] = new Item(i, listaPastasTermicas.get(i).getNome(), listaPastasTermicas.get(i));
        }
        Item i = (Item) JOptionPane.showInputDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarPastaTermica", "Escolher")+":", Dicionario.getSignificado(Dicionario.PATH+"EditarPastaTermica", "Titulo"), JOptionPane.QUESTION_MESSAGE,
                null, itemsEventos, itemsEventos[0]);
        if (i != null) {
            return (PastaTermica) i.getObject();
        }
        return null;
    }
}
