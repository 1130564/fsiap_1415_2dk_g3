package fsiap2dkg3.ui;

import java.util.List;
import javax.swing.JOptionPane;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.RemoverPastaTermicaController;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.model.PastaTermica;
import utils.Item;
import utils.Utils;

/**
 * Remover Dissipador UI
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class RemoverPastaTermicaUI {

    private RemoverPastaTermicaController controller;

    public RemoverPastaTermicaUI() {
        this.controller = new RemoverPastaTermicaController();
    }

    /**
     * Correr Caso de Uso
     *
     * @return Sucesso do Caso de Uso
     */
    public boolean run() {
        PastaTermica pt = selecionaPastaTermica();
        if (pt == null) {
            return false;
        }

        this.controller.setPastaTermica(pt);
        if (Utils.confirmar() && this.controller.removerPastaTermica()) {
            if (!Dicionario.getLANG().equals("en_gb")) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Remover pasta térmica com Sucesso!", "Remover Pasta Termica", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Thermal Compound successfully removed!", "Remove Thermal Compound", JOptionPane.INFORMATION_MESSAGE);
            }
            return true;
        }
        if (!Dicionario.getLANG().equals("en_gb")) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Remover pasta térmica sem Sucesso!", "Remover Pasta Termica", JOptionPane.WARNING_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Insucess! Thermal Compound was not removed!", "Remove Thermal Compound", JOptionPane.WARNING_MESSAGE);
        }
        return false;
    }

    private PastaTermica selecionaPastaTermica() {
        List<PastaTermica> listaPastasTermicas = this.controller.getPastasTermicas();
        if (listaPastasTermicas.isEmpty()) {
            if (!Dicionario.getLANG().equals("en_gb")) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Não existem pastas térmicas para Remover!", "Remover Pasta Termica", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "There are no thermal compounds to remove!", "Remove Thermal Compound", JOptionPane.INFORMATION_MESSAGE);
            }
            return null;
        }
        Object itemsEventos[] = new Object[listaPastasTermicas.size()];
        for (int i = 0; i < itemsEventos.length; i++) {
            itemsEventos[i] = new Item(i, listaPastasTermicas.get(i).getNome(), listaPastasTermicas.get(i));
        }
        Item it;
        if (!Dicionario.getLANG().equals("en_gb")) {
            it = (Item) JOptionPane.showInputDialog(VARS.CURRENTFRAME, "Escolha a pasta térmica:*", "Remover Pasta Termica", JOptionPane.QUESTION_MESSAGE,
                    null, itemsEventos, itemsEventos[0]);
        } else {
            it = (Item) JOptionPane.showInputDialog(VARS.CURRENTFRAME, "Select Thermal Compound:*", "Remove Thermal Compound", JOptionPane.QUESTION_MESSAGE,
                    null, itemsEventos, itemsEventos[0]);
        }
        if (it != null) {
            return (PastaTermica) it.getObject();
        }
        return null;
    }
}
