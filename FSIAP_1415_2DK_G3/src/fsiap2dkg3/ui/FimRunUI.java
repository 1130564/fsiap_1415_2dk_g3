package fsiap2dkg3.ui;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.FimRunController;
import fsiap2dkg3.dicionario.Dicionario;
import javax.swing.JOptionPane;

/**
 * (Fim da Aplicação)Fim Run UI
 *
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ruben Ribeiro <1130629@isep.ipp.pt>
 * @author Diogo Vieira <1121248@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * @author Ana Pacheco <1130679@isep.ipp.pt>
 * @version 1.0
 */
public class FimRunUI {

    private FimRunController controller;

    public FimRunUI() {
        this.controller = new FimRunController();
    }

    public boolean run() {

        if (VARS.ALTERACOES) {
            int resposta = 2;
            if (!VARS.AUTOSAVE) {
                if (Dicionario.getLANG().equals("en_gb")) {
                    String[] op = {"Cancel", "Exit", "Save and Exit"};
                    resposta = JOptionPane.showOptionDialog(VARS.CURRENTFRAME,
                            "Do you wish to close the application?\n"
                            + "(the data were not yet saved)",
                            "Exit",
                            0,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            op,
                            op[0]);
                } else {
                    String[] op = {"Cancelar", "Sair", "Guardar e Sair"};
                    resposta = JOptionPane.showOptionDialog(VARS.CURRENTFRAME,
                            "Deseja Sair da Aplicação?\n"
                            + "(os dados ainda não foram guardados)",
                            "Sair",
                            0,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            op,
                            op[0]);
                }

            }
            if (resposta == 2) {
                boolean returnSave = controller.save();
                if (!returnSave) {
                    JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "ERRO: Configurações Basicas de Sistema", "Guardar Configurações Basicas de Sistema", JOptionPane.ERROR_MESSAGE);
                } else {
                    return true;
                }
            } else if (resposta == 1) {
                return true;
            }
        } else {
            int resposta;
            if (Dicionario.getLANG().equals("en_gb")) {
                String[] op = {"Cancel", "Exit"};
                resposta = JOptionPane.showOptionDialog(VARS.CURRENTFRAME,
                        "Do you wish to close the application?",
                        "Exit",
                        0,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        op,
                        op[0]);
            } else {
                String[] op = {"Cancelar", "Sair"};
                resposta = JOptionPane.showOptionDialog(VARS.CURRENTFRAME,
                        "Deseja Sair da Aplicação?",
                        "Sair",
                        0,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        op,
                        op[0]);
            }

            if (resposta == 1) {
                return true;
            }
        }
        return false;
    }
}
