/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.ui;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.BackupCarregarController;
import java.io.File;
import javax.swing.JOptionPane;
import utils.Utils;

/**
 * UC13 - Persistência dos Dados(Carregar) UI
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class BackupCarregarUI {
     private BackupCarregarController controller;

    public BackupCarregarUI() {
        this.controller = new BackupCarregarController();
    }

    public boolean run() {
        File file = Utils.fileChooser(VARS.CURRENTFRAME, "Carregar Backup...", "Carregar Backup", VARS.SISTEMAFILE, Utils.TypeChooser.OPEN);
        if (file != null) {
            boolean returnCarregarBackup = controller.carregarBackup(file);
            if (!returnCarregarBackup) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "ERRO: Ficheiro Inválido e/ou Corrompido", "Carregar Backup", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Backup com Sucesso!", "Carregar Backup", JOptionPane.INFORMATION_MESSAGE);
                return true;
            }
        }
        return false;
    }
}
