package fsiap2dkg3.ui;

import java.util.List;
import javax.swing.JOptionPane;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.RemoverDissipadorController;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.model.Dissipador;
import utils.Item;
import utils.Utils;

/**
 * Remover Dissipador UI
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class RemoverDissipadorUI {

    private RemoverDissipadorController controller;

    public RemoverDissipadorUI() {
        this.controller = new RemoverDissipadorController();
    }

    /**
     * Correr Caso de Uso
     *
     * @return Sucesso do Caso de Uso
     */
    public boolean run() {
        Dissipador dissipador = selecionaDissipador();
        if (dissipador == null) {
            return false;
        }

        this.controller.setDissipador(dissipador);
        if (Utils.confirmar() && this.controller.removerDissipador()) {
            if (!Dicionario.getLANG().equals("en_gb")) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Remover dissipador com Sucesso!", "Remover Dissipador", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Cooler successfully removed", "Remove cooler", JOptionPane.INFORMATION_MESSAGE);

            }
            return true;
        }
        if (!Dicionario.getLANG().equals("en_gb")) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Remover  sem Sucesso!", "Remover Dissipador", JOptionPane.WARNING_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Cooler successfully removed", "Remove cooler", JOptionPane.INFORMATION_MESSAGE);

        }
        return false;
    }

    private Dissipador selecionaDissipador() {
        List<Dissipador> listaDissipadores = this.controller.getDissipadores();
        if (listaDissipadores.isEmpty()) {
            if (!Dicionario.getLANG().equals("en_gb")) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Não Existe dissipadores para Remover!", "Remover Dissipador", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "There is no coolers to remove", "Remove Cooler", JOptionPane.INFORMATION_MESSAGE);

            }

            return null;
        }
        Object itemsEventos[] = new Object[listaDissipadores.size()];
        for (int i = 0; i < itemsEventos.length; i++) {
            itemsEventos[i] = new Item(i, listaDissipadores.get(i).getNome(), listaDissipadores.get(i));
        }
        Item i ;
        if (!Dicionario.getLANG().equals("en_gb")) {
            i = (Item) JOptionPane.showInputDialog(VARS.CURRENTFRAME, "Escolha o Dissipador:*", "Remover Dissipador", JOptionPane.QUESTION_MESSAGE,
                    null, itemsEventos, itemsEventos[0]);
        } else {
          i = (Item) JOptionPane.showInputDialog(VARS.CURRENTFRAME, "Select a cooler", "Remove cooler", JOptionPane.QUESTION_MESSAGE,
                    null, itemsEventos, itemsEventos[0]);
        }
                
        if (i != null) {
            return (Dissipador) i.getObject();
        }
        return null;
    }
}
