/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.ui;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.AdicionarDissipadorController;
import fsiap2dkg3.controller.AdicionarPastaTermicaController;
import javax.swing.JOptionPane;

import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.gui.DialogAdicionarDissipador;
import fsiap2dkg3.gui.DialogAdicionarPastaTermica;

/**
 * Adicionar Dissipador UI
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class AdicionarPastaTermicaUI {

    private AdicionarPastaTermicaController controller;

    public AdicionarPastaTermicaUI() {
        this.controller = new AdicionarPastaTermicaController();
    }

    /**
     * Correr Caso de Uso
     *
     * @return Sucesso do Caso de Uso
     */
    public boolean run() {
        DialogAdicionarPastaTermica dialog = new DialogAdicionarPastaTermica(null, true, this.controller);
        boolean returnDialogAdicionarDissipador = (boolean) dialog.getInformation();

        if (returnDialogAdicionarDissipador) {  //ADICIONAR NO DICIONARIO!
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH + "AdicionarPastaTermica", "Sucesso"), Dicionario.getSignificado(Dicionario.PATH + "AdicionarPastaTermica", "Titulo"), JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH + "AdicionarPastaTermica", "Insucesso"), Dicionario.getSignificado(Dicionario.PATH + "AdicionarPastaTermica", "Titulo"), JOptionPane.WARNING_MESSAGE);
        }

        return returnDialogAdicionarDissipador;
    }
}
