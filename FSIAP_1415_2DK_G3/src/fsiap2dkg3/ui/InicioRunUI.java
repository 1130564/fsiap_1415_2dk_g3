/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.ui;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.InicioRunController;
import javax.swing.JOptionPane;

/**
 *
 * @author gilbertopereira
 */
public class InicioRunUI {
    private InicioRunController controller;

    public InicioRunUI() {
        this.controller = new InicioRunController();
    }
    
    public boolean run() {
        boolean returnLoad = this.controller.load();
        if (!returnLoad) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "ERRO: Configurações Basicas de Sistema", "Carregar Configurações Basicas de Sistema", JOptionPane.ERROR_MESSAGE);
        }
        return returnLoad;
    }
}
