/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.ui;


import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.EditarDissipadorController;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.gui.DialogEditarDissipador;
import fsiap2dkg3.model.Dissipador;
import utils.Item;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Editar Dissipador UI
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class EditarDissipadorUI {
    
    
    private EditarDissipadorController controller;
    
    public EditarDissipadorUI(){
        this.controller=new EditarDissipadorController();
    }
    /**
     * Correr Caso de Uso
     *
     * @return Sucesso do Caso de Uso
     */
    public boolean run() {
        Dissipador dissipador = selecionaDissipador();
        if (dissipador == null) {
            return false;
        }

        this.controller.setDissipador(dissipador);
        
        DialogEditarDissipador dialogEditarDissipador = new DialogEditarDissipador(VARS.CURRENTFRAME, true, this.controller);
        boolean returnDialogEditarDissipador= (boolean) dialogEditarDissipador.getInformation();
       
        if (returnDialogEditarDissipador) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarDissipador", "Sucesso"), Dicionario.getSignificado(Dicionario.PATH+"EditarDissipador", "Titulo"), JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarDissipador", "Insucesso"), Dicionario.getSignificado(Dicionario.PATH+"EditarDissipador", "Titulo"), JOptionPane.WARNING_MESSAGE);
        }
        
        return returnDialogEditarDissipador;
    }

    
    private Dissipador selecionaDissipador() {
        List<Dissipador> listaDissipadores = this.controller.getDissipadores();
        if (listaDissipadores.isEmpty()) {  //ALTERAR DICIONARIO!!
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarDissipador", "InvalidaListaDissipador")+"!", Dicionario.getSignificado(Dicionario.PATH+"EditarDissipador", "Titulo"), JOptionPane.INFORMATION_MESSAGE);
            return null;
        }
        Object itemsEventos[] = new Object[listaDissipadores.size()];
        for (int i = 0; i < itemsEventos.length; i++) {
            itemsEventos[i] = new Item(i, listaDissipadores.get(i).getNome(), listaDissipadores.get(i));
        }
        Item i = (Item) JOptionPane.showInputDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarDissipador", "Escolher")+":", Dicionario.getSignificado(Dicionario.PATH+"EditarDissipador", "Titulo"), JOptionPane.QUESTION_MESSAGE,
                null, itemsEventos, itemsEventos[0]);
        if (i != null) {
            return (Dissipador) i.getObject();
        }
        return null;
    }
}
