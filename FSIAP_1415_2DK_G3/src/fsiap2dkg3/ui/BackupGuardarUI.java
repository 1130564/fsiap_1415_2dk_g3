package fsiap2dkg3.ui;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.BackupGuardarController;
import java.io.File;
import javax.swing.JOptionPane;
import utils.Utils;

/**
 * UC12 - Persistência dos Dados(Guardar) UI
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class BackupGuardarUI {
    private BackupGuardarController controller;

    public BackupGuardarUI() {
        this.controller = new BackupGuardarController();
    }

    public boolean run() {
        File file = Utils.fileChooser(VARS.CURRENTFRAME, "Guardar Backup...", "Guardar Backup", VARS.SISTEMAFILE, Utils.TypeChooser.SAVE);
        if (file != null) {
            boolean returnGuardarBackup = controller.guardarBackup(file);
            if (!returnGuardarBackup) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "ERRO: Ficheiro Inválido e/ou Corrompido", "Guardar Backup", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Backup com Sucesso!", "Guardar Backup", JOptionPane.INFORMATION_MESSAGE);
                return true;
            }
        }
        return false;
    }
}