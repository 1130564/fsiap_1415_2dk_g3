package fsiap2dkg3.ui;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.EditarProcessadorController;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.gui.DialogEditarProcessador;
import fsiap2dkg3.model.Processador;
import utils.Item;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * UC2 - Editar Processador UI
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class EditarProcessadorUI {
    
    
    private EditarProcessadorController controller;
    
    public EditarProcessadorUI(){
        this.controller=new EditarProcessadorController();
    }
    /**
     * Correr Caso de Uso
     *
     * @return Sucesso do Caso de Uso
     */
    public boolean run() {
        Processador processador = selecionaProcessador();
        if (processador == null) {
            return false;
        }

        this.controller.setProcessador(processador);
        
        DialogEditarProcessador dialogEditarProcessador = new DialogEditarProcessador(VARS.CURRENTFRAME, true, this.controller);
        boolean returnDialogEditarProcessado = (boolean) dialogEditarProcessador.getInformation();
       
        if (returnDialogEditarProcessado) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarProcessador", "Sucesso"), Dicionario.getSignificado(Dicionario.PATH+"EditarProcessador", "Titulo"), JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarProcessador", "Insucesso"), Dicionario.getSignificado(Dicionario.PATH+"EditarProcessador", "Titulo"), JOptionPane.WARNING_MESSAGE);
        }
        
        return returnDialogEditarProcessado;
    }

    
    private Processador selecionaProcessador() {
        List<Processador> listaProcessadores = this.controller.getProcessadores();
        if (listaProcessadores.isEmpty()) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarProcessador", "InvalidaListaProcessador")+"!", Dicionario.getSignificado(Dicionario.PATH+"EditarProcessador", "Titulo"), JOptionPane.INFORMATION_MESSAGE);
            return null;
        }
        Object itemsEventos[] = new Object[listaProcessadores.size()];
        for (int i = 0; i < itemsEventos.length; i++) {
            itemsEventos[i] = new Item(i, listaProcessadores.get(i).getNome(), listaProcessadores.get(i));
        }
        Item i = (Item) JOptionPane.showInputDialog(VARS.CURRENTFRAME, Dicionario.getSignificado(Dicionario.PATH+"EditarProcessador", "Escolher")+":*", Dicionario.getSignificado(Dicionario.PATH+"EditarProcessador", "Titulo"), JOptionPane.QUESTION_MESSAGE,
                null, itemsEventos, itemsEventos[0]);
        if (i != null) {
            return (Processador) i.getObject();
        }
        return null;
    }
}
