package fsiap2dkg3.ui;

import java.util.List;
import javax.swing.JOptionPane;

import fsiap2dkg3.VARS;
import fsiap2dkg3.controller.RemoverProcessadorController;
import fsiap2dkg3.dicionario.Dicionario;
import fsiap2dkg3.model.Processador;
import utils.Item;
import utils.Utils;

/**
 * UC3 - Remover Processador UI
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class RemoverProcessadorUI {

    private RemoverProcessadorController controller;

    public RemoverProcessadorUI() {
        this.controller = new RemoverProcessadorController();
    }

    /**
     * Correr Caso de Uso
     *
     * @return Sucesso do Caso de Uso
     */
    public boolean run() {
        Processador processador = selecionaProcessador();
        if (processador == null) {
            return false;
        }

        this.controller.setProcessador(processador);
        if (Utils.confirmar() && this.controller.removerProcessador()) {
            if (!Dicionario.getLANG().equals("en_gb")) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Remover Processador com Sucesso!", "Remover Processador", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Processor successfully removed!", "Remove Processor", JOptionPane.INFORMATION_MESSAGE);

            }

            return true;
        }
        if (!Dicionario.getLANG().equals("en_gb")) {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Remover Processador sem Sucesso!", "Remover Processador", JOptionPane.WARNING_MESSAGE);

        } else {
            JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Processor not removed!", "Remove Processor", JOptionPane.INFORMATION_MESSAGE);

        }

        return false;
    }

    private Processador selecionaProcessador() {
        List<Processador> listaProcessadores = this.controller.getProcessadores();
        if (listaProcessadores.isEmpty()) {
            if (!Dicionario.getLANG().equals("en_gb")) {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "Não Existe Processadores para Remover!", "Remover Processadador", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(VARS.CURRENTFRAME, "There are no processor's to remove", "Remove Processor", JOptionPane.INFORMATION_MESSAGE);

            }
           
            return null;
        }
        Object itemsEventos[] = new Object[listaProcessadores.size()];
        for (int i = 0; i < itemsEventos.length; i++) {
            itemsEventos[i] = new Item(i, listaProcessadores.get(i).getNome(), listaProcessadores.get(i));
        }
        Item i;
              
        if (!Dicionario.getLANG().equals("en_gb")) {
                i = (Item) JOptionPane.showInputDialog(VARS.CURRENTFRAME, "Escolha o Processador:", "Remover Processadador", JOptionPane.QUESTION_MESSAGE,
                null, itemsEventos, itemsEventos[0]);
            } else {
               i = (Item) JOptionPane.showInputDialog(VARS.CURRENTFRAME, "Choose the processor:", "Remove processor", JOptionPane.QUESTION_MESSAGE,
                null, itemsEventos, itemsEventos[0]);

            }
        if (i != null) {
            return (Processador) i.getObject();
        }
        return null;
    }
}
