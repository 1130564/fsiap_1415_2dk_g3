package fsiap2dkg3.dicionario;

import java.util.Locale;

/**
 * Idioma
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class Idioma {
    /**
     * Idioma
     */
    private String idioma;
    
    /**
     * TAG do Idioma
     */
    private String tag;
    
    /**
     * Construir Instância do Idioma (COMPLETO)
     *
     * @param idioma Idioma
     * @param tag TAG do Idioma
     */
    public Idioma(String idioma,String tag){
        this.idioma=idioma;
        this.tag=tag;
    }
    
     /**
     * Construir Instância do Idioma (CÓPIA)
     *
     * @param copia Idioma com as Características a Copiar
     */
    public Idioma(Idioma copia){
        this(copia.idioma,copia.tag);
    }
    
    /**
     * Consultar Idioma
     * @return Idioma
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * Consultar Tag do Idioma
     * @return Tag
     */
    public String getTag() {
        return tag;
    }
    
    /**
     * Consultar Locale do Idioma
     * @return Locale
     */
    public Locale getLocale(){
        return new Locale(this.tag);
    }

    /**
     * Alterar Idioma
     * @param idioma Novo Idioma
     */
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    /**
     * Alterar TAG do Idioma
     * @param tag Nova Tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }
    
    /**
     * Representação Textual da Instância Formatada
     *
     * @return Representação Textual do Idioma
     */
    @Override
    public String toString() {
        return this.idioma+"{"+this.tag+"}";
    }
    
     /**
     * Compara o Idioma com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Idioma equivalente
     * ao Idioma. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        Idioma i = (Idioma) outro;
        return this.idioma.equals(i.idioma)
               && this.tag.equals(i.tag); 
    }
    
    

}
