package fsiap2dkg3.dicionario;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Dicionário
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class Dicionario {
    private Dicionario(){}
    
    /**
     * Idiomas Suportados 
     */
    public static final List<Idioma> IDIOMAS = new ArrayList<>(Arrays.asList(
            new Idioma("Português","pt_PT")
            , new Idioma("Inglês","en_GB")
    ));
    
    /**
     * Idioma Actual do Dicionário
     */
    private static Locale LANG=Locale.getDefault();
   
    /**
     * Localização dos Dicionários Pré-Definida
     */
    public static String PATH ="fsiap2dkg3/dicionario/";
    
    /**
     * Dicionário Pré-Definido
     */
    public static String DICIONARIO = "Dicionario";
    
    /**
     * Consultar Idioma Actual
     * @return Idioma
     */
    public static String getLANG(){
        return Dicionario.LANG.toString();
    }
    
    /**
     * Consultar Idioma Locale Actual
     * @return Idioma
     */
    public static Locale getLANGLocale(){
        return Dicionario.LANG;
    }
    
    /**
     * Alterar Idioma Actual
     * @param lang Novo Idioma
     */
    public static void setLANG(String lang){
        Dicionario.LANG=new Locale(lang);
        Locale.setDefault(Dicionario.LANG);
    }
    
    /**
     * Alterar Locale Idioma Actual
     * @param lang Novo Locale Idioma
     */
    public static void setLANGLocale(Locale lang){
        Dicionario.LANG=lang;
        Locale.setDefault(Dicionario.LANG);
    }
    
    /**
     * Consultar Dicionario
     * @param fileDicionario Ficheiro do Dicionario
     * @param lang Idioma do Dicionario
     * @return Dicionario
     */
    public static ResourceBundle getDicionario(String fileDicionario, Locale lang){
        return ResourceBundle.getBundle(fileDicionario,lang);
    }
    
    /**
     * Consultar Dicionario no Ideoma Actual
     * @param fileDicionario Ficheiro do Idioma
     * @return Dicionario
     */
    public static ResourceBundle getDicionario(String fileDicionario){
        return Dicionario.getDicionario(fileDicionario,Dicionario.LANG);
    }
    
    /**
     * Consultar Dicionario Pré-Definido
     * @param lang Idioma do Dicionario
     * @return Dicionario
     */
    public static ResourceBundle getDicionario(Locale lang){
        return Dicionario.getDicionario(Dicionario.PATH+Dicionario.DICIONARIO,Dicionario.LANG);
    }
    
    /**
     * Consultar Dicionario Pré-Definido com o Idioma Actual
     * @return Dicionario
     */
    public static ResourceBundle getDicionario(){
        return Dicionario.getDicionario(Dicionario.PATH+Dicionario.DICIONARIO,Dicionario.LANG);
    }
    
    /**
     * Consultar Significado no Dicionario com Palavra Key no Idioma Lang
     * @param fileDicionario Ficheiro do Dicionario
     * @param key Key da Palavra no Dicionario
     * @param lang Ideioma do Dicionario
     * @return Significado
     */
    public static String getSignificado(String fileDicionario, String key, Locale lang){
        return Dicionario.getDicionario(fileDicionario, lang).getString(key);
    }
    
    /**
     * Consultar Significado no Dicionário com Palavra Key no Idioma Actual
     * @param fileDicionario Ficheiro do Dicionário
     * @param key Key da Palavra no Dicionario
     * @return Significado
     */
    public static String getSignificado(String fileDicionario,String key){
        return Dicionario.getDicionario(fileDicionario).getString(key);
    }
    
    /**
     * Consultar Significado no Dicionário Pré-Defindio com Palavra Key no Ideioma lang
     * @param key Key da Palavra no Dicionario
     * @param lang Ideioma do Dicionario
     * @return Significado
     */
    public static String getSignificado(String key,Locale lang){
        return Dicionario.getDicionario(lang).getString(key);
    }
    
    /**
     * Consultar Significado Dicionário Pré-Definido com Palavra Key e Idioma Actual
     * @param key Key da Palavra no Dicionario
     * @return Significado
     */
    public static String getSignificado(String key){
        return Dicionario.getDicionario().getString(key);
    }
    
}
