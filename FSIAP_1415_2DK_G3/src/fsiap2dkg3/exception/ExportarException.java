package fsiap2dkg3.exception;

/**
 * Exceção de Exportar para Ficheiro
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class ExportarException extends Exception{
   public ExportarException(){
        
    }
    public ExportarException(String mensagem){
        super(mensagem);
    }
}
