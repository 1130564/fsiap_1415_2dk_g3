package fsiap2dkg3.exception;

/**
 * Exceção de Importar de Ficheiro
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class ImportarException extends Exception{
   public ImportarException(){
        
    }
    public ImportarException(String mensagem){
        super(mensagem);
    }
}