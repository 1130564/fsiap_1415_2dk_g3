package fsiap2dkg3.importar.sistema;

import fsiap2dkg3.VARS;
import fsiap2dkg3.exception.ImportarException;
import fsiap2dkg3.exportar.relatorio.ExportarRelatorio;
import fsiap2dkg3.model.Sistema;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Date;

/**
 * Importar Sistema em Formato Binario
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class ImportarSistemaBAK implements ImportarSistemaType{
    private final String type = "BAK";

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public ImportarSistemaData importar(File file) throws ImportarException {
        return this.importar(file, true);
    }

    @Override
    public ImportarSistemaData importar(File file, boolean log) throws ImportarException {
        String erro = "";
        Sistema sistema = null;
        try {
            FileInputStream fileIn = new FileInputStream(file.getAbsoluteFile());
            ObjectInputStream inStream = new ObjectInputStream(fileIn);
            sistema = (Sistema) inStream.readObject();
            inStream.close();
            fileIn.close();
        } catch (Exception ex) {
            erro = ex.getMessage();
        }
        ExportarRelatorio.exportar(VARS.LOGFILE, "[IMPORTAR];[SISTEMA];[" + file.getAbsolutePath() + "];[BAK];[" + new Date() + "]" + "[" + (erro.equals("") ? "OK" : "ERRO(" + erro + ")") + "]", log);
        if (erro.equals("")) {
            return new ImportarSistemaData(sistema);
        }
        throw new ImportarException(erro);
    }

}