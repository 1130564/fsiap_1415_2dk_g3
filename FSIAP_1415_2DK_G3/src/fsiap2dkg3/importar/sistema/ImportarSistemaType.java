package fsiap2dkg3.importar.sistema;

import java.io.File;

import fsiap2dkg3.exception.ImportarException;

/**
 * Importar Sistema (ESTRUTURA)
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public interface ImportarSistemaType {
    public String getType();
    public ImportarSistemaData importar(File file) throws ImportarException;
    public ImportarSistemaData importar(File file, boolean log) throws ImportarException;
}
