package fsiap2dkg3.importar.sistema;

import fsiap2dkg3.model.Sistema;

/**
 * DATA do Importar Sistema
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class ImportarSistemaData {
    /**
     * Sistema do Importar Sistema
     */
    private Sistema sistema;

    /**
     * Construir Instância do Importar Sistema
     *
     * @param sistema Sistema
     */
    public ImportarSistemaData(Sistema sistema) {
        this.sistema = sistema;
    }

    /**
     * Consultar Sistema
     *
     * @return Sistema
     */
    public Sistema getSistema() {
        return this.sistema;
    }

    /**
     * Alterar Sistema
     *
     * @param sistema Novo Sistema
     */
    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    /**
     * Compara o Importar Sistema com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Importar Evento
     * equivalente ao Importar Evento. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        ImportarSistemaData ie = (ImportarSistemaData) outro;
        return this.equals(ie.getSistema());
    }

}
