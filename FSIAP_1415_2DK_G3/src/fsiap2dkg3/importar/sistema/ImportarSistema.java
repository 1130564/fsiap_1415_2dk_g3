/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fsiap2dkg3.importar.sistema;

import fsiap2dkg3.VARS;
import fsiap2dkg3.exception.ExtensaoException;
import fsiap2dkg3.exception.ImportarException;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import utils.Utils;

/**
 *
 * @author gilbertopereira
 */
public class ImportarSistema {
     public final static List<ImportarSistemaType> TYPES = new ArrayList<ImportarSistemaType>(Arrays.asList(
            new ImportarSistemaBAK()
    ));

    public static ImportarSistemaData importar(File file) throws ExtensaoException, ImportarException{
        return importar(file, VARS.LOG);
    }
    
    public static ImportarSistemaData importar(File file, boolean log) throws ExtensaoException, ImportarException {
        String ext = Utils.getExtensao(file);
        if (ext != null) {
            for (ImportarSistemaType type : TYPES) {
                if (type.getType().equalsIgnoreCase(ext)) {
                    return type.importar(file, log);
                }
            }
        }
        throw new ExtensaoException("Extensão Inválida");
    }
}
