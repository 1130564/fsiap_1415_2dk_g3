package fsiap2dkg3.hclc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fsiap2dkg3.model.PastaTermica;

/**
 * Registo de Pastas Térmicas
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class RegistoPastasTermicas implements Serializable {

    // Variáveis de Instância \\
    /**
     * Lista de Pastas Térmicas
     */
    private List<PastaTermica> listaPastasTermicas;
    
    // Constantes de Classe \\
    // Variáveis de Classe \\
    /**
     * Lista de Pastas Térmicas Pré-Definida
     */
    public static final List<PastaTermica> LISTAPASTASTERMICAS = new ArrayList<>();

    // Construtores \\
    /**
     * Construir Instância do Registo de Pastas Térmicas (COMPLETO)
     *
     * @param listaPastasTermicas Registo de Pastas Térmicas
     */
    public RegistoPastasTermicas(List<PastaTermica> listaPastasTermicas) {
        this.setPastasTermicas(listaPastasTermicas);
    }

    /**
     * Construir Instância do Registo de Pastas Térmicas (CÓPIA)
     *
     * @param copia Registo de Pastas Térmicas com as Características a Copiar
     */
    public RegistoPastasTermicas(RegistoPastasTermicas copia) {
        this(copia.listaPastasTermicas);
    }

    /**
     * Construir Instância do Registo de Pastas Térmicas (PRÉ-DEFINIDO)
     */
    public RegistoPastasTermicas() {
        this(RegistoPastasTermicas.LISTAPASTASTERMICAS);
    }

    // Métodos de Instância \\
    /**
     * Consultar Lista de Pastas Térmicas
     *
     * @return Lista de Pastas Térmicas
     */
    public List<PastaTermica> getPastasTermicas() {
        return new ArrayList<>(this.listaPastasTermicas);
    }

    /**
     * Consultar Pasta Térmica na Posição n
     *
     * @param pos Posição do PastaTermica
     * @return Pasta Térmica N
     */
    public PastaTermica getPastaTermica(int pos) {
        return this.listaPastasTermicas.get(pos);
    }

    /**
     * Alterar Lista de Pastas Térmicas
     *
     * @param listaPastasTermicas Nova Lista de Pastas Térmicas
     */
    public final void setPastasTermicas(List<PastaTermica> listaPastasTermicas) {
        if (listaPastasTermicas == null) {
            throw new IllegalArgumentException("Lista de Pastas Termicas Ínvalido");
        }
        this.listaPastasTermicas = rmDuplicatedPastasTermicas(listaPastasTermicas);
    }

    /**
     * Representação Textual da Instância Registo de Pastas Térmicas
     *
     * @return Representação Textual da Registo de Pastas Térmicas
     */
    @Override
    public String toString() {
        String str = "";
        for (PastaTermica o : this.listaPastasTermicas) {
            str += "Pasta Térmica: \n" + o.toString() + "\n";
        }
        return str;
    }

    /**
     * Compara o Registo Pastas Termicas com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Registo Pastas
     * Termicas equivalente ao Registo Pastas Termicas. Caso contrário, retorna
     * false.
     */
    @Override
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        RegistoPastasTermicas la = (RegistoPastasTermicas) outro;
        if (this.numeroPastasTermicas() == la.numeroPastasTermicas()) {
            return this.listaPastasTermicas.containsAll(la.listaPastasTermicas);
        }
        return false;
    }

    /**
     * Adicionar uma Pasta Térmica, único é valido, na Lista de Pastas Térmicas
     *
     * @param o Pasta Térmica a Adicionar
     * @return Sucesso da Operação
     */
    public boolean addPastaTermica(PastaTermica o) {
        if (this.validaPastaTermica(o)) {
            return this.listaPastasTermicas.add(o);
        }
        return false;
    }

    /**
     * Remover uma Pasta Térmica da Lista de Pastas Térmicas
     *
     * @param o Pasta Térmica a Remover
     * @return Sucesso da Operação
     */
    public boolean rmPastaTermica(PastaTermica o) {
        return this.listaPastasTermicas.remove(o);
    }

    /**
     * Remover uma Pasta Térmica da Lista de Pastas Térmicas
     *
     * @param pos Posição da Pasta Térmica a Remover
     * @return Pasta Térmica Removido
     */
    public PastaTermica rmPastaTermica(int pos) {
        return this.listaPastasTermicas.remove(pos);
    }

    /**
     * Substituir Pasta Térmicas
     *
     * @param old Antiga Pasta Térmica
     * @param novo Novo Pasta Térmica
     * @return Sucesso da Operação
     */
    public boolean replacePastaTermica(PastaTermica old, PastaTermica novo) {
        if (this.validaPastaTermica(novo) && this.contemPastaTermica(old)) {
            this.listaPastasTermicas.set(this.indexOfPastaTermica(old), novo);
            return true;
        }
        return false;
    }

    /**
     * Editar Pasta Térmica na Posição N
     *
     * @param pos Posição da Pasta Térmica
     * @param nome Novo Nome da Pasta Térmica
     * @param condutividadeTermica Nova Condutividade Termica da Pasta
     * Térmica(W/m.c)
     * @param espessura Nova Espessura da Pasta Térmica(m)
     * @return Sucesso da Operação
     */
    public boolean editPastaTermica(int pos, String nome, float condutividadeTermica, float espessura) {
        PastaTermica old = this.getPastaTermica(pos);
        return this.editPastaTermica(old, nome, condutividadeTermica, espessura);
    }

    /**
     * Editar Pasta Térmica
     *
     * @param o Pasta Térmica
     * @param nome Novo Nome da Pasta Térmica
     * @param condutividadeTermica Nova Condutividade Termica da Pasta
     * Térmica(W/m.c)
     * @param espessura Nova Espessura Termica da Pasta Térmica(m)
     * @param area Nova Area da Pasta Térmica(m^2)
     * @return Sucesso da Operação
     */
    public boolean editPastaTermica(PastaTermica o, String nome, float condutividadeTermica, float espessura) {
        PastaTermica novo = RegistoPastasTermicas.novoPastaTermica(nome, condutividadeTermica, espessura);
        return this.replacePastaTermica(o, novo);
    }

    /**
     * Numero de Pastas Térmicas da Lista de Pastas Térmicas
     *
     * @return Numero de Pastas Térmicas
     */
    public int numeroPastasTermicas() {
        return this.listaPastasTermicas.size();
    }

    /**
     * Posição da Pasta Térmica na Lista de Pastas Térmicas
     *
     * @param o Pasta Térmica a Procurar
     * @return Posição
     */
    public int indexOfPastaTermica(PastaTermica o) {
        return this.listaPastasTermicas.indexOf(o);
    }

    /**
     * Verificar se uma Pasta Térmica está Contido na Lista de Pastas Térmicas
     *
     * @param o Pasta Térmica a Verificar
     * @return true se contiver o Pasta Térmica. Caso Contrário, retorna false.
     */
    public boolean contemPastaTermica(PastaTermica o) {
        return this.listaPastasTermicas.contains(o);
    }

    /**
     * Validar Pasta Térmica
     *
     * @param o Pasta Térmica a Validar
     * @return true se for uma Pasta Térmica valido e único na Lista de Pastas
     * Termicas. Caso contrário, retorna false.
     */
    public boolean validaPastaTermica(PastaTermica o) {
        return o.valida() && this.validaGlobalPastaTermica(o);
    }

    // Métodos de Classe \\
    /**
     * Criar uma Nova Pasta Térmica
     *
     * @return Nova Pasta Térmica
     */
    public static PastaTermica novoPastaTermica() {
        return new PastaTermica();
    }

    /**
     * Criar uma Nova Pasta Térmica
     *
     * @param nome Nome da Pasta Térmica
     * @param condutividadeTermica Condutividade Termica da Pasta Térmica(W/m.c)
     * @param espessura Espessura Termica da Pasta Térmica(m)
     * @param area Area da Pasta Térmica(m^2)
     * @return Nova Pasta Térmica
     */
    public static PastaTermica novoPastaTermica(String nome, float condutividadeTermica, float espessura) {
        return new PastaTermica(nome, condutividadeTermica, espessura);
    }

    // Métodos Privados \\
    /**
     * Validar existência da Pasta Térmica na Lista de Pastas Térmicas
     *
     * @param o Pasta Térmica a Verifica
     * @return true se não existir. Caso contrário, retorna false.
     */
    private boolean validaGlobalPastaTermica(PastaTermica o) {
        return (!this.contemPastaTermica(o));
    }

    /**
     * Remover Pastas Térmicas Idênticas
     *
     * @param listaPastasTermicas Lista de Pastas Térmicas
     * @return Nova Lista de Pastas Térmicas
     */
    private static List<PastaTermica> rmDuplicatedPastasTermicas(List<PastaTermica> listaPastasTermicas) {
        ArrayList<PastaTermica> la = new ArrayList<>();
        for (PastaTermica a : listaPastasTermicas) {
            if (!la.contains(a)) {
                la.add(a);
            }
        }
        return la;
    }
}
