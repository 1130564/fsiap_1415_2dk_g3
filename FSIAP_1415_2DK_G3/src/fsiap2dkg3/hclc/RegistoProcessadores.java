package fsiap2dkg3.hclc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fsiap2dkg3.model.Processador;

/**
 * Registo de Processadores
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class RegistoProcessadores implements Serializable {

    // Variáveis de Instância \\
    /**
     * Lista de Processadores
     */
    private List<Processador> listaProcessadores;
    
    // Constantes de Classe \\
    // Variáveis de Classe \\
    /**
     * Lista de Processadores Pré-Definida
     */
    public static final List<Processador> LISTAPROCESSADORES = new ArrayList<>();

    // Construtores \\
    /**
     * Construir Instância do Registo de Processadores (COMPLETO)
     *
     * @param listaProcessadores Lista de Processadores
     */
    public RegistoProcessadores(List<Processador> listaProcessadores) {
        this.setPastasTermicas(listaProcessadores);
    }

    /**
     * Construir Instância do Registo de Processadores (CÓPIA)
     *
     * @param copia Registo de Processadores com as Características a Copiar
     */
    public RegistoProcessadores(RegistoProcessadores copia) {
        this(copia.listaProcessadores);
    }

    /**
     * Construir Instância do Registo de Processadores (PRÉ-DEFINIDO)
     */
    public RegistoProcessadores() {
        this(RegistoProcessadores.LISTAPROCESSADORES);
    }

    // Métodos de Instância \\
    /**
     * Consultar Lista de Processadores
     *
     * @return Lista de Processadores
     */
    public List<Processador> getProcessadores() {
        return new ArrayList<>(this.listaProcessadores);
    }

    /**
     * Consultar Processador na Posição n
     *
     * @param pos Posição do Processador
     * @return Processador N
     */
    public Processador getProcessador(int pos) {
        return this.listaProcessadores.get(pos);
    }

    /**
     * Alterar Lista de Processadores
     *
     * @param listaProcessadores Nova Lista de Processadores
     */
    public final void setPastasTermicas(List<Processador> listaProcessadores) {
        if (listaProcessadores == null) {
            throw new IllegalArgumentException("Lista de Processadores Ínvalido");
        }
        this.listaProcessadores = rmDuplicatedPastasTermicas(listaProcessadores);
    }

    /**
     * Representação Textual da Instância Registo de Processadores
     *
     * @return Representação Textual da Registo de Processadores
     */
    @Override
    public String toString() {
        String str = "";
        for (Processador o : this.listaProcessadores) {
            str += "Processador: \n" + o.toString() + "\n";
        }
        return str;
    }

    /**
     * Compara o Lista de Processadores com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Lista de Processadores
     * equivalente ao Lista de Processadores. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        RegistoProcessadores la = (RegistoProcessadores) outro;
        if (this.numeroProcessadores()== la.numeroProcessadores()) {
            return this.listaProcessadores.containsAll(la.listaProcessadores);
        }
        return false;
    }

    /**
     * Adicionar uma Processador, único é valido, na Lista de Processadores
     *
     * @param o Processador a Adicionar
     * @return Sucesso da Operação
     */
    public boolean addProcessador(Processador o) {
        if (this.validaProcessador(o)) {
            return this.listaProcessadores.add(o);
        }
        return false;
    }

    /**
     * Remover uma Processador da Lista de Processadores
     *
     * @param o Processador a Remover
     * @return Sucesso da Operação
     */
    public boolean rmProcessador(Processador o) {
        return this.listaProcessadores.remove(o);
    }

    /**
     * Remover uma Processador da Lista de Processadores
     *
     * @param pos Posição da Processador a Remover
     * @return Processador Removido
     */
    public Processador rmProcessador(int pos) {
        return this.listaProcessadores.remove(pos);
    }

    /**
     * Substituir Processadors
     *
     * @param old Antiga Processador
     * @param novo Novo Processador
     * @return Sucesso da Operação
     */
    public boolean replaceProcessador(Processador old, Processador novo) {
        if (this.validaProcessador(novo) && this.contemProcessador(old)) {
            this.listaProcessadores.set(this.indexOfProcessador(old), novo);
            return true;
        }
        return false;
    }
    
    /**
     * Editar Processador na Posição N
     * @param pos Posição da Processador
     * @param nome Novo Nome da Processador
     * @param potenciaDissipada (TDP)Potencia Dissipada do Processador(W)
     * @param maxTemperatura Termperatura Maxima do Processador(ºC)
     * @param area Nova Area da Processador(m^2)
     * @return Sucesso da Operação
     */
    public boolean editProcessador(int pos,String nome, float potenciaDissipada, float maxTemperatura, float area){
        Processador old = this.getProcessador(pos);
        return this.editProcessador(old, nome, potenciaDissipada, maxTemperatura, area);
    }
    /**
     * Editar Processador
     * @param o Processador
     * @param nome Novo Nome da Processador
     * @param potenciaDissipada (TDP)Potencia Dissipada do Processador(W)
     * @param maxTemperatura Termperatura Maxima do Processador(ºC)
     * @param area Area da Processador(m^2)
     * @return Sucesso da Operação
     */
    public boolean editProcessador(Processador o,String nome, float potenciaDissipada, float maxTemperatura, float area){
        Processador novo = RegistoProcessadores.novoProcessador(nome, potenciaDissipada, maxTemperatura, area);
        return this.replaceProcessador(o, novo);
    }

    /**
     * Numero de Processadores da Lista de Processadores
     *
     * @return Numero de Processadores
     */
    public int numeroProcessadores() {
        return this.listaProcessadores.size();
    }

    /**
     * Posição da Processador na Registo de Processadores
     *
     * @param o Processador a Procurar
     * @return Posição
     */
    public int indexOfProcessador(Processador o) {
        return this.listaProcessadores.indexOf(o);
    }

    /**
     * Verificar se uma Processador está Contido na Lista de Processadores
     *
     * @param o Processador a Verificar
     * @return true se contiver o Processador. Caso Contrário, retorna false.
     */
    public boolean contemProcessador(Processador o) {
        return this.listaProcessadores.contains(o);
    }
    
    /**
     * Validar Processador
     *
     * @param o Processador a Validar
     * @return true se for uma Processador valido e único na Lista de
     * Processadores. Caso contrário, retorna false.
     */
    public boolean validaProcessador(Processador o) {
        return o.valida() && this.validaGlobalProcessador(o);
    }

    // Métodos de Classe \\
    /**
     * Criar uma Nova Processador
     *
     * @return Nova Processador
     */
    public static Processador novoProcessador() {
        return new Processador();
    }

    /**
     * Criar uma Nova Processador
     *
     * @param nome Nome da Processador
     * @param potenciaDissipada (TDP)Potencia Dissipada do Processador(W)
     * @param maxTemperatura Termperatura Maxima do Processador(ºC)
     * @param area Area da Processador(m^2)
     * @return Nova Processador
     */
    public static Processador novoProcessador(String nome, float potenciaDissipada, float maxTemperatura, float area) {
        return new Processador(nome, potenciaDissipada, maxTemperatura, area);
    }

    // Métodos Privados \\
    /**
     * Validar existência da Processador na Lista de Processadores
     *
     * @param o Processador a Verifica
     * @return true se não existir. Caso contrário, retorna false.
     */
    private boolean validaGlobalProcessador(Processador o) {
        return (!this.contemProcessador(o));
    }

    /**
     * Remover Processadores Idênticas
     *
     * @param listaProcessadores Lista de Processadores
     * @return Nova Lista de Processadores
     */
    private static List<Processador> rmDuplicatedPastasTermicas(List<Processador> listaProcessadores) {
        ArrayList<Processador> la = new ArrayList<>();
        for (Processador a : listaProcessadores) {
            if (!la.contains(a)) {
                la.add(a);
            }
        }
        return la;
    }
}
