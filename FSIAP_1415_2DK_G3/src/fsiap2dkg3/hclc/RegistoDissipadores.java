package fsiap2dkg3.hclc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fsiap2dkg3.model.Dissipador;

/**
 * Registo de Dissipadores
 *
 * @author Emanuel Marques <1130553@isep.ipp.pt>
 * @author Vitor Moreira <1130564@isep.ipp.pt>
 * @author Paulo Andrade <1130313@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @version 1.0
 */
public class RegistoDissipadores implements Serializable {

    // Variáveis de Instância \\
    /**
     * Lista de Dissipadores
     */
    private List<Dissipador> listaDissipadores;
    
    // Constantes de Classe \\
    // Variáveis de Classe \\
    /**
     * Lista de Dissipadores Pré-Definida
     */
    public static final List<Dissipador> LISTADISSIPADORES = new ArrayList<>();

    // Construtores \\
    /**
     * Construir Instância do Registo de Dissipadores (COMPLETO)
     *
     * @param listaDissipadores Lista de Dissipadores
     */
    public RegistoDissipadores(List<Dissipador> listaDissipadores) {
        this.setDissipadores(listaDissipadores);
    }

    /**
     * Construir Instância do Registo de Dissipadores (CÓPIA)
     *
     * @param copia Registo de Dissipadores com as Características a Copiar
     */
    public RegistoDissipadores(RegistoDissipadores copia) {
        this(copia.listaDissipadores);
    }

    /**
     * Construir Instância do Registo de Dissipadores (PRÉ-DEFINIDO)
     */
    public RegistoDissipadores() {
        this(RegistoDissipadores.LISTADISSIPADORES);
    }

    // Métodos de Instância \\
    /**
     * Consultar Lista de Dissipadores
     *
     * @return Lista de Dissipadores
     */
    public List<Dissipador> getDissipadores() {
        return new ArrayList<>(this.listaDissipadores);
    }

    /**
     * Consultar Dissipador na Posição n
     *
     * @param pos Posição do Dissipador
     * @return Dissipador N
     */
    public Dissipador getDissipador(int pos) {
        return this.listaDissipadores.get(pos);
    }

    /**
     * Alterar Lista de Dissipadores
     *
     * @param listaDissipadores Nova Lista de Dissipadores
     */
    public final void setDissipadores(List<Dissipador> listaDissipadores) {
        if (listaDissipadores == null) {
            throw new IllegalArgumentException("Lista de Dissipadores Ínvalido");
        }
        this.listaDissipadores = rmDuplicatedDissipadores(listaDissipadores);
    }

    /**
     * Representação Textual da Instância Registo de Dissipadores
     *
     * @return Representação Textual da Registo de Dissipadores
     */
    @Override
    public String toString() {
        String str = "";
        for (Dissipador o : this.listaDissipadores) {
            str += "Dissipador: \n" + o.toString() + "\n";
        }
        return str;
    }

    /**
     * Compara o Registo de Dissipadores com outro Objecto
     *
     * @param outro Objecto a Comparar
     * @return true se o objecto recebido representar outro Registo de Dissipadores
     * equivalente ao Registo de Dissipadores. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outro) {
        if (this == outro) {
            return true;
        }
        if (outro == null || this.getClass() != outro.getClass()) {
            return false;
        }
        RegistoDissipadores la = (RegistoDissipadores) outro;
        if (this.numeroDissipadores() == la.numeroDissipadores()) {
            return this.listaDissipadores.containsAll(la.listaDissipadores);
        }
        return false;
    }

    /**
     * Adicionar um Dissipador, único é valido, a Lista de Dissipadores
     *
     * @param o Dissipador a Adicionar
     * @return Sucesso da Operação
     */
    public boolean addDissipador(Dissipador o) {
        if (this.validaDissipador(o)) {
            return this.listaDissipadores.add(o);
        }
        return false;
    }

    /**
     * Remover um Dissipador da Lista de Dissipadores
     *
     * @param o Dissipador a Remover
     * @return Sucesso da Operação
     */
    public boolean rmDissipador(Dissipador o) {
        return this.listaDissipadores.remove(o);
    }

    /**
     * Remover um Dissipador da Lista de Dissipadores
     *
     * @param pos Posição do Dessipador a Remover
     * @return Dissipador Removido
     */
    public Dissipador rmDissipador(int pos) {
        return this.listaDissipadores.remove(pos);
    }

    /**
     * Substituir Dissipadores
     *
     * @param old Antigo Dissipador
     * @param novo Novo Dissipador
     * @return Sucesso da Operação
     */
    public boolean replaceDissipador(Dissipador old, Dissipador novo) {
        if (this.validaDissipador(novo) && this.contemDissipador(old)) {
            this.listaDissipadores.set(this.indexOfDissipador(old), novo);
            return true;
        }
        return false;

    }
    
    /**
     * Editar Dissipador na Posição N
     * @param pos Posição do Dissipador
     * @param nome Novo Nome do Dissipador
     * @param tipo Novo Tipo do Dissipador
     * @param condutividadeTermica Nova Condutividade Termica do Dissipador(W/m.c)
     * @param espessura Nova Espessura Termica do Dissipador(m)
     * @return Sucesso da Operação
     */
    public boolean editDissipador(int pos,String nome, String tipo, float condutividadeTermica, float espessura){
        Dissipador old = this.getDissipador(pos);
        return this.editDissipador(old, nome, tipo, condutividadeTermica, espessura);
    }
    /**
     * Editar Dissipador
     * @param o Dissipador
     * @param nome Novo Nome do Dissipador
     * @param tipo Novo Tipo do Dissipador
     * @param condutividadeTermica Nova Condutividade Termica do Dissipador(W/m.c)
     * @param espessura Nova Espessura Termica do Dissipador(m)
     * @return Sucesso da Operação
     */
    public boolean editDissipador(Dissipador o,String nome,String tipo,float condutividadeTermica, float espessura){
        Dissipador novo = RegistoDissipadores.novoDissipador(nome, tipo, condutividadeTermica, espessura);
        return this.replaceDissipador(o, novo);
    }

    /**
     * Numero de Dissipadores da Lista de Dissipadores
     *
     * @return Numero de Dissipadores
     */
    public int numeroDissipadores() {
        return this.listaDissipadores.size();
    }

    /**
     * Posição do Dissipador na Lista de Dissipadores
     *
     * @param o Dissipador a Procurar
     * @return Posição
     */
    public int indexOfDissipador(Dissipador o) {
        return this.listaDissipadores.indexOf(o);
    }

    /**
     * Verificar se um Dissipador está Contido na Lista de Dissipador
     *
     * @param o Dissipador a Verificar
     * @return true se contiver o Dissipador. Caso Contrário, retorna false.
     */
    public boolean contemDissipador(Dissipador o) {
        return this.listaDissipadores.contains(o);
    }
    
    /**
     * Validar Dissipador
     *
     * @param o Dissipador a Validar
     * @return true se for um um Dissipador valido e único na Lista de
     * Dissipadores. Caso contrário, retorna false.
     */
    public boolean validaDissipador(Dissipador o) {
        return o.valida() && this.validaGlobalDissipador(o);
    }

    // Métodos de Classe \\
    /**
     * Criar um Novo Dissipador
     *
     * @return Novo Dissipador
     */
    public static Dissipador novoDissipador() {
        return new Dissipador();
    }

    /**
     * Criar um Novo Dissipador
     *
     * @param nome Nome do Dissipador
     * @param tipo Tipo do Dissipador
     * @param condutividadeTermica Condutividade Termica do Dissipador(W/m.c)
     * @param espessura Espessura Termica do Dissipador(m)
     * @param area Area (m^2)
     * @return Novo Dissipador
     */
    public static Dissipador novoDissipador(String nome, String tipo, float condutividadeTermica, float espessura) {
        return new Dissipador(nome, tipo, condutividadeTermica, espessura);
    }

    // Métodos Privados \\
    /**
     * Validar existência do Dissipador na Lista de Dissipadores
     *
     * @param o Dissipador a Verifica
     * @return true se não existir. Caso contrário, retorna false.
     */
    private boolean validaGlobalDissipador(Dissipador o) {
        return (!this.contemDissipador(o));
    }

    /**
     * Remover Dissipadores Idênticos
     *
     * @param listaDissipadores Lista de Dissipadores
     * @return Nova Lista de Dissipadores
     */
    private static List<Dissipador> rmDuplicatedDissipadores(List<Dissipador> listaDissipadores) {
        ArrayList<Dissipador> la = new ArrayList<>();
        for (Dissipador a : listaDissipadores) {
            if (!la.contains(a)) {
                la.add(a);
            }
        }
        return la;
    }
}
