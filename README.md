A nossa aplicação consiste em, dado um processador, a sua temperatura máxima, a 
temperatura ambiente e depois os outros componentes que ajudam a dissipar o calor
proveniente do processador, pasta térmica e dissipador, testar essa combinação e mostrar ao 
utilizador se a combinação resulta ou não, isto é, se a temperatura máxima do processador 
não é excedida, prevenindo assim a danificação de componentes e assegurando um bom 
funcionamento do sistema. 

Read More [WIKI Repository FSIAP2DKG3...](https://bitbucket.org/1130564/fsiap_1415_2dk_g3/wiki/Home)
## Grupo 2DK-G3:
* Emanuel Marques - <1130553@isep.ipp.pt>
* Vitor Moreira - <1130564@isep.ipp.pt>
* Paulo Andrade - <1130313@isep.ipp.pt>
* Gilberto Pereira - <1131251@isep.ipp.pt>

Copyright © 2014/2015 2DK-G3. All rights reserved.